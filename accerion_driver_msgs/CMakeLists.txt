cmake_minimum_required(VERSION 2.8.3)
project(accerion_driver_msgs)

find_package(catkin REQUIRED COMPONENTS
  std_msgs
  geometry_msgs
  message_generation
  actionlib_msgs
)

add_message_files(
   FILES
   SensorDiagnostics.msg
   DriftCorrectionDetails.msg
   DirSizePart.msg
   CreatedSignature.msg
   RamRomStats.msg
   SearchRange.msg
)

add_service_files(
  FILES
  Cluster.srv
  EmptyCommand.srv
  Event.srv
  SetBufferedRecoveryBufferLength.srv
  StartBufferedRecoveryMode.srv
  ModeCommand.srv
  ModeClusterCommand.srv
  RequestCommand.srv
  ParentChildFrames.srv
  Pose.srv
  UDPSettings.srv
  RecordingsList.srv
  DeleteArea.srv
  SetIP.srv
  DateTime.srv
  GetAreas.srv
  SetArea.srv
  RamRomStatsSRV.srv
)

add_action_files(DIRECTORY action
  FILES
  SearchLoopClosures.action
  GetMap.action
  Recordings.action
  SendMap.action
  SubsetMap.action
  GetLogs.action
  UpdateSensor.action
)

generate_messages(
   DEPENDENCIES
   std_msgs
   geometry_msgs
   actionlib_msgs
)

catkin_package(CATKIN_DEPENDS std_msgs geometry_msgs message_runtime actionlib_msgs)
