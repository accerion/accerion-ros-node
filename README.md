# Package Summary
This repo provides two nodes:

**accerion_driver**
- Interfacing with Triton sensor (toggle functionality, setup connection type,
  sending/receiving data via actions and service)
  

**accerion_pose_manager**
- Easy-to-use way to implement Triton into your mobile robot platform
- Manages incoming data (odometry, Lidar, Triton drift corrections)
- Processes incoming data and returns the robot pose, relevant TF's
- Can be customized to your needs or used as a template to build your own pose manager

For more information on the Accerion pose manager, read the README in accerion_pose_manager.

Additional notes:

- the accerion_driver and accerion_pose_manager nodes are tested on ROS Melodic and Noetic
- the accerion_driver uses jsk_rviz_plugins (http://wiki.ros.org/jsk_rviz_plugins) for visualization.

# 1. Initial setup

## Adding accerion-ros-node into an existing catkin workspace
Clone this repository into the `src/` folder of your catkin workspace, change your working directory into the repository and update the required submodules:

```bash
cd src/accerion-ros-node
git submodule update --init --recursive
```

Now build your packages by running:

```bash
catkin build
```



- Make sure that the build completes without errors.
- Next to the `src/` folder, new folders `build_isolated/` and `devel/` should have been created in the previous step.
- Source the `devel/setup.bash` file to include this workspace in your environment
```bash
source devel/setup.bash
```
- Alternative to the previous step, you can also add the following line to your `~/.bashrc` to source your workspace at each terminal start (replace `your-user-name` with your actual username)
```bash
source /home/your-user-name/catkin_ws/devel/setup.bash
```
Then just reopen a terminal and continue to next step, or in your current terminal, do:
```bash
source ~/.bashrc
```

- To verify that your setup is complete, do:
```bash
rospack find accerion_driver
```
The response should be:
```bash
/home/your-user-name/codes/catkin_ws/src/accerion-ros-node/accerion_driver
```

# 2. Launch files

## 2.1. Sample launch
If you have an Accerion sensor running in the same network as your ROS master, do:

`roslaunch accerion_driver demo.launch`

to bring up the Accerion ROS Driver and RViz visualization for a sample demonstration usage.

If you have multiple Accerion sensors in your local network, you need to specify the serial number of the sensor by using the `serial_number` argument as:

`roslaunch accerion_driver demo.launch serial_number:=598100101`

Note: If you do not specify the `serial_number`, it will be assigned `0` by default. As a result, the driver will pair with the first Accerion sensor it detects in the network.

## 2.2. accerion_driver
In order to see the Triton axis moving in rviz, enable internal mode from the Accerion GUI.

The package `accerion_driver` contains the following launch files:
- `demo.launch`: launches the `accerion_driver` and `accerion_visualization` nodes and launches rviz via the `accerion_rviz.launch` file. Includes `triton_description.launch` file.
- `triton_pose_manager_mapping.launch`: example launch file for mapping using laser positioning. In the example file, a laser is positioning the robot using landmarks. The laser provides the `map` to `odom` transform.
- `triton_pose_manager_mapping_amcl.launch`: example launch file for mapping using amcl for positioning the robot and providing the `map` to `odom` transform. The robot has both `base_footprint` and `base_link` frames. 
- `triton_pose_manager_localization.launch`: example launch file to use when localizing. Independent of whether amcl is used to estimate the robot's position or laser positioning is used. If the robot is providing the `odom` to `base_link` transform, set `publish_global_tf` to `false`.
- `accerion_rviz.launch`: launches rviz with the `accerion_demo.rviz` file.

## 2.3. accerion_triton_description
The package `accerion_triton_description` contains the following launch files:
- `triton_description`: launches the joint_state_publisher and robot_state_publisher nodes with the `accerion_triton.xml` file. Remaps `robot_description` to `triton_description` and `joint_states` to `triton_joint_states`. As the file `accerion_triton.xml` does not contains a single link, the nodes will not publish anything and only the Triton mesh will be displayed at the origin coordinates in rviz.

# 3. Nodes

## 3.1. accerion_driver

### 3.1.1. Subscribed Topics
**/$node_namespace/ext_ref**
- Type: geometry_msgs/PoseWithCovarianceStamped  
- Triton depends on external pose measurements in both Mapping and Localization phase (see Accerion's technology concept). Triton listens to this topic for these external pose measurements.

**/$node_namespace/reset_pose_manual**
- Type: geometry_msgs/PoseStamped
- The pose of Triton can be reset by publishing a message on this topic (note that Triton pose will be overwritten again if you publish to the /$node_namespace/ext_ref topic).

### 3.1.2. Published Topics
**/$node_namespace/odom**
- Type: nav_msgs/Odometry
- Relative position of the sensor in the relative reference frame (default: odom)

**/$node_namespace/corrected_odom**
- Type: nav_msgs/Odometry
- Position of the sensor in the global reference frame (default: map)

**/$node_namespace/correction_details**
- Type: accerion_driver_msgs/DriftCorrectionDetails
- Details of the drift correction

**/$node_namespace/diagnostics**
- Type: accerion_driver_msgs/SensorDiagnostics
- Custom diagnostics details from the sensor

**/$node_namespace/line_follower**
- Type: geometry_msgs/PoseArray
- The pose (position and orientation) of two points in global coordinate system. One is the sensor's output, the other is the closest point on a line cluster

**/$node_namespace/line_follower_visualization**
- Type: geometry_msgs/PolygonStamped
- Two points (no orientation) in global coordinate system. One is the sensor's output, the other is the closest point on a line cluster

**/$node_namespace/corrections**
- Type: nav_msgs/Odometry  
- Drift corrections based on the floor map. This is the corrected pose for the sensor's output in the global reference frame (default: map). Use this to reset the external reference pose

**/$node_namespace/map**
- Type: sensor_msgs/PointCloud2
- The floor map which contains virtual signatures

**/$node_namespace/marker_in_sensor_frame**
- Type: visualization_msgs/MarkerArray
- Marker array published in the global sensor coordinate system

### 3.1.3. Params
**serial_number (integer)**
- Serial number of Triton (if "0", uses first Triton detected)

**connection_type (string, default: "CONNECTION_TCP")**
- The connection type between accerion_driver and Triton. Can take the following values: "CONNECTION_UDP_BROADCAST", "CONNECTION_UDP_UNICAST", "CONNECTION_TCP_DISABLE_UDP", "CONNECTION_TCP"

**global_parent_frame (string, default "map")**
- Global parent frame

**sensor_link (string, default "sensor_link")**
- Triton frame

**local_parent_frame (string, default "odom")**
- Local parent frame 

**ext_ref_topic (string, default "ext_ref")**
- Topic used by Triton to receive the external reference

**ext_ref_timestamp_compensation (bool, default True)**
- Latency compensation by computing a time offset from the difference between incoming external reference messages and current ROS time.

**ext_ref_latency (float, default 0.0)**
- Parameter to compensate for any remaining latency present in the system (using Triton's visual odometry). This time offset (in milliseconds) value is combined with latency compensation from ROS timestamps if ext_ref_timestamp_compensation is enabled. In case ROS timestamp compensation is disabled only the value specific to this parameter will be used for latency compensation.
  Contact Accerion before using this.

**search_range_topic (string, default "search_range")**
- Topic used by Triton to receive the angle and radius to search in

**local_ip (string, default "0.0.0.0")**
- Specify the local IP address with connection type "CONNECTION_UDP_UNICAST". 

### 3.1.4. Services
Map management: 
- /delete_cluster 
- /get_areas
- /set_area
- /delete_area

Sensor settings: 
- /set_datetime 
- /set_ip 
- /set_pose 
- /set_udp_settings 
- /set_internal_mode

Other: 
- /mode 
- /set_buffer_length
- /get_buffer_length
- /start_recovery
- /stop_recovery
- /mode_with_cluster 
- /recordings 
- /request 

Maintenance: 
- /get_ram_rom_stats

# Actions
PGO/g2o:
- /search_loop_closures 

Map management:
- /subset_map
- /get_map 
- /send_map

Maintenance:
- /recordings
- /get_logs 
- /update_sensor

# Detailed explanation of each service
**/delete_all_clusters** 

The service above requires one boolean argument. Note that the parameter is currently ignored. As the name suggests this service deletes all the clusters from the sensor database. 

**/delete_cluster** 

This service enables you to delete a single cluster from the database by providing a single uint32 as an argument. E.g.: 
```bash 
rosservice call /$node_namespace/delete_cluster 123
```

**/get_areas**

This service returns the currently active area ID and a list of available area IDs.
```bash
rosservice call /$node_namespace/get_areas "{}"
```

**/set_area**

This service sets the active area ID. Area ID is of type uint16.
```bash
rosservice call /$node_namespace/set_area "desiredArea: 1"
```

**/set_datetime** 

This service allows you to set the date and time of the sensor. It requires a string for the date in the following format: dd/mm/yyyy and the time in the following format: hh:mm:ss. E.g.: 
```bash
rosservice call /$node_namespace/set_datetime "date: '11/03/2021' time: '10:34:00'" 
```
Make sure to use the correct quotes so that the date and time are correctly parsed as strings. 

**/set_internal_mode** 

This service enables you to, as the name suggest, enable the internal mode. The request takes one boolean parameter that should reflect the on/off state you desire. E.g.: 
```bash
rosservice call /$node_namespace/set_internal_mode true
```

**/set_ip** 

For setting the ip you can use this service. It requires 3 parameters; the ip address, the netmask and the gateway. All in the following format x.x.x.x. E.g.: 
```bash
rosservice call /$node_namespace/set_ip 192.168.0.10 255.255.255.0 192.168.0.1
```

**/set_pose** 

This service can be used to set the sensor pose. The expected 3 arguments are all float32 and are (in order) x(m), y(m) and theta(deg). E.g.: 

```bash
rosservice call /$node_namespace/set_pose 10 10 0
```

**/set_udp_settings** 

For setting the UDP settings, this service can be used. It allows you to set which message are to be sent over UDP and whether the messages should be broadcasted or unicasted (with or without heartbeat) and the desired unicast IP. The parameters are as follows; first there are 4 fields for the unicast ip address separated by spaces, then follows the message type (1 = none, 2 = streaming, 3 = intermitted, 4 = both), then follows the udp mode (1 = broadcast, 2 = unicast, 3 = unicast with disabled heartbeat). Note that the heartbeat on option 3 is only disabled until the next reboot. 

An example of this command is

```bash
rosservice call /$node_namespace/set_udp_settings 192 168 0 1 4 1
```

**/mode** 

This service is a bit more generic and its use is determined by the first
parameter; it is a string that should contain one of the following values:
`localization`, `idle`, `recording`, `reboot`, `aruco` or `continuous_signature_updating`.
The second argument is another string that should contain either `start` or
`stop`. Combining these parameters, you can start or stop various modes. E.g.: 

```bash
rosservice call /$node_namespace/mode idle start
```

**/mode/set_buffered_recovery_buffer_length**

This service can be used to set the buffer lenght of the buffered recovery mode. E.g.:
```bash
rosservice call /$node_namespace/mode/set_buffered_recovery_buffer_length "bufferLength: 1.0" 
```

**/mode/get_buffered_recovery_buffer_length**

This service can be used to check the currently set buffer lenght of the buffered recovery mode. E.g.:
```bash
rosservice call /$node_namespace/mode/get_buffered_recovery_buffer_length "{}"
```

**/mode/start_buffered_recovery**

This service can be used to start buffered recovery mode by specifying the x and y anchor positions and the search radius, all being of type float32. The `fullMapSearch` boolean parameter specifies if full map search mode is on. E.g.:
```bash
rosservice call /$node_namespace/mode/start_buffered_recovery "xPos: 0.0
yPos: 0.0
radius: 0.0
fullMapSearch: false"
```

**/mode/stop_buffered_recovery**

This service is used to stop the buffered recovery mode. E.g.:
```bash
rosservice call /$node_namespace/mode/stop_buffered_recovery "{}" 
```

**/mode_with_cluster** 

The mode with cluster service enables you to toggle modes in which a cluster has to be specified. The first parameter specifies the mode; mapping, line_following or active_mapping. The second parameter is of type int32 and should contain the desired cluster. The last parameter is of type string and should contain either start or stop to toggle the mode. E.g.: 

```bash
rosservice call /$node_namespace/mode_with_cluster mapping 123 start
```

**/recordings** 

This service is intended to interact with the recordings that are present on the sensor (These are made by toggling recording mode, see /mode). The request has 3 parameters; the first one being a string that contains one of three modes; list, download or delete. The list returns a list of names of the recordings that are present (see recordings variable in response), download enables you to download one or more recordings and delete allows you to delete one or more recordings. The second argument is an array of indexes and the last argument is a string that should contain the path for where to store the recordings. Below you’ll find an example for each available mode: 

```bash
rosservice call /$node_namespace/recordings list [] /
```

```bash
rosservice call /$node_namespace/recordings download [0] /home/user/recording.arf
```

```bash
rosservice call /$node_namespace/recordings delete [0] /
```

**/request**

The request service can be used to request various things. This should be specified in the first and only parameter of this request. It is of type string and should contain one of the following values: map, version, version_hash, ip_address, serial_number, correction_counter or acks. E.g.:
```bash
rosservice call /$node_namespace/request version 
```

**/get_ram_rom_stats**

This service allows to check the current RAM and ROM usage of the sensor. It consists of some basic info in the struct and has an array/vector with some additional parts. For further clarification it is best to check the API. The output is a boolean indicating the success/failure of the request and the RamRomStats message(Identical to API, but implemented as a ROS message). To get this information, simply make an empty request E.g.:
```bash
rosservice call /$node_namespace/get_ram_rom_stats "{}"
```

# Detailed explanation of actions

Actions can be triggered by publishing on corresponding topics. E.g. for /search_loop_closures:
```
rostopic pub /$node_namespace/search_loop_closures/goal accerion_driver_msgs/SearchLoopClosuresActionGoal '{goal:{data: 1, search_radius: 0.25}}'

```
If the search radius is not provided, a 0.0 value is sent to the sensor, which converts it to the default value of 0.5 meters.

Results and feedback from the actions can be viewed by echoing the corresponding topic.

The "topics" exposed for the actions are as follows:
```bash
/$node_namespace/$action_name/cancel
```
```bash
/$node_namespace/$action_name/feedback
```
```bash
/$node_namespace/$action_name/goal
```
```bash
/$node_namespace/$action_name/result
```
```bash
/$node_namespace/$action_name/status
```

Note that not all topics are implemented. To check what messages are available you can use:
```
rosmsg show accerion_driver_msgs/SearchLoopClosuresAction
```
This will print the available `SearchLoopClosuresActionX` messages (Goal, Result, Feedback) and their corresponding fields. You need to be running accerion_driver node to be able to show those messages.

**/search_loop_closures**

This action requires a boolean argument true/false to either start/stop the searching of loop closures. During this, the progress will be printed if you echo the `/$node_namespace/search_loop_closures/feedback` topic and a notification will be given when done on the `/$node_namespace/search_loop_closures/result` msg. The sensor will, as the name suggests, internally search for loopclosures to provide a usable g2o map that can be used to optimize it. 

**/subset_map**

This action enables you to subset, or basically copy a part of, the map/database to a new area. It requires you to enter a formatted string with the desired clusterID's of the current active area and a single target area ID. The latter should be a unused area ID. The former accepts comma separated values, but ranges are accepted too. It should be noted that a warning will be shown when the request failed OR when the request succeeded but one or more of the requested clusterID's did not exist and were left out. It is highly recommended to look up a ROS action tutorial if unfamiliar with the concept. 

Note that cancel is not implemented.

**/get_map**

This action enables you to download the map/database from a sensor and requires a string that contains the path where the map/database should be stored.

**/send_map**

This action enables you to upload/import the map/database to/in a sensor and requires a string that contains the path where the map/database file is stored and an integer that should be set to the dsdired placement strategy. 0 Will replace, 1 will merge and 2 will update the map.

**/get_logs**

The logs action enables the retrieval of logs. The request knows one parameter; a string containing the path where they should be stored. The second boolean parameter is deprecated and is ignored.

**/update_sensor** 

This action has one purpose; updating the sensor. To do this simply make a request with the path to the update file(.asu) as the first parameter.

**/recordings**

Action implementation of the recordings service.

As with the service, use:
- `list` to get list of recordings; 
- `download` to get one or more recordings;
- `delete` delete one or more recordings;

# FAQ
UDP settings:
- If you do not see any messages on the '/triton/corrections' topic coming in, check if the UDP settings are set to 'Broadcast', 'streaming and intermittant' via the following rosservice call:

rosservice call /triton/set_udp_settings "unicast_ip_address: ''
message_type: 0x04
udp_mode: 1"

# Parameter dependence between pose manager and accerion driver
The accerion_driver and the pose manager nodes have overlapping parameters and parameters that depend on the other node, namelly:
* accerion_driver: `global_parent_frame` -> pose_manager: `global_parent_frame`
* accerion_driver: `local_parent_frame` -> pose_manager: `local_parent_frame`
* accerion_driver: `sensor_link` -> pose_manager: `sensor_link`
* accerion_driver: `ext_ref_topic` -> pose_manager: `ext_ref_topic`
* accerion_driver: `search_range_topic` -> pose_manager: `search_range_topic`

# 4. Meshes
The Triton meshes are provided in the `accerion_triton_description` package in folder `meshes`. The files are provided in `.stl` format and teir named according to the revision version of Triton. The cordinate system origin is in the middle of the bottom plate of Triton's body, without considering the connectors.

# 5. Tests
To run the tests for the different packages do: `catkin test <package_name>` after building the catkin workspace. If `CATKIN_ENABLE_TESTING` is turned off during build, `catkin test` won’t be able to build and consequently execute the tests. In that case it will return: `make: *** No rule to make target 'run_tests'.  Stop.`.

To build the tests without running them (what happens when you call `catkin test`) you can use the `--catkin-make-args tests` flag when building your package, i.e. `catkin build <package_name> --catkin-make-args tests`.

The tests launch files can be triggered individually from the folder they are located: `rostest --reuse-master --text visualization_ros_utest.test`. Use `--reuse-master` to connect to an already running roscore. This is useful for debugging as it allows for monitoring the test nodes and topics. Use `--text` to display extra information in the console while running the test.

If the tests fail you can look in `build/<package_name>/test_results/<package_name>/roslaunch-check_launch.xml` or in the logs folder.

Launch files are tested with the `roslaunch_add_file_check(launch USE_TEST_DEPENDENCIES)` command in the CMakeList.txt file of the packages containing such files.
