/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include "AccerionDriverROS.h"

#include <cstdint>
#include <ros/ros.h>
#include <tf/tf.h>

#include "test_ros_common.hpp"


using CovarianceMatrix = std::array<double, 36>;
using BitsTranslationMap = std::map<std::uint8_t, std::string>;

class AccerionDriverTest: public AccerionDriverROS, public ::testing::Test, public ::testing::WithParamInterface<int> {
public:
    AccerionDriverTest(): AccerionDriverROS::AccerionDriverROS() {};

    void EqualityCheckBetweenPoses(const geometry_msgs::Pose& ros_pose, const accerion::api::Pose& expected_pose) const {
        accerion::api::Pose received_pose = {ros_pose.position.x,
                                             ros_pose.position.y,
                                             tf::getYaw(ros_pose.orientation) * accerion::api::kRadToDeg};
        
        EXPECT_NEAR(received_pose.x, expected_pose.x, common_ros_test::kEps) << "Expected X value: " << expected_pose.x << "; received X value: " << received_pose.x << std::endl;
        EXPECT_NEAR(received_pose.y, expected_pose.y, common_ros_test::kEps) << "Expected Y value: " << expected_pose.y << "; received Y value: " << received_pose.y << std::endl;
        EXPECT_DOUBLE_EQ(ros_pose.position.z, 0.0) << "Expected X value: " << 0.0 << "; received Z value: " << ros_pose.position.z << std::endl;
        
        EXPECT_NEAR(received_pose.th, expected_pose.th, common_ros_test::kEps) << "Expected TH value: " << expected_pose.th << "; received TH value: " << received_pose.th << std::endl;
    }

    void EqualityCheckBetweenVelocities(const geometry_msgs::Twist& received_twist, const std::vector<double>& expected_values) const {
        EXPECT_NEAR(received_twist.linear.x, expected_values[0], common_ros_test::kEps) << "Expected linear X value: " << expected_values[0] << "; received X value: " << received_twist.linear.x << std::endl;
        EXPECT_NEAR(received_twist.linear.y, expected_values[1], common_ros_test::kEps) << "Expected linear Y value: " << expected_values[1] << "; received Y value: " << received_twist.linear.y << std::endl;
        EXPECT_DOUBLE_EQ(received_twist.linear.z, 0.0) << "Expected linear Z value: " << 0.0 << "; received Z value: " << received_twist.linear.z << std::endl;

        EXPECT_DOUBLE_EQ(received_twist.angular.x, 0.0) << "Expected angular X value: " << 0.0 << "; received X value: " << received_twist.angular.x << std::endl;
        EXPECT_DOUBLE_EQ(received_twist.angular.y, 0.0) << "Expected angular Y value: " << 0.0 << "; received Y value: " << received_twist.angular.y << std::endl;
        EXPECT_NEAR(received_twist.angular.z, expected_values[2], common_ros_test::kEps) << "Expected angular Z value: " << expected_values[2] << "; received Z value: " << received_twist.angular.z << std::endl;
    }

    void NearEqualityCheckOfCovarianceMatrix(const geometry_msgs::PoseWithCovariance::_covariance_type received_values, const CovarianceMatrix& expected_values) const {
        auto near_equality_comparison = [this](const double& x, const double& y){
            const bool result {(x-y) < common_ros_test::kEps};
            if (!result) {
                std::cout << "Expected covariance value: " << y << "; received value: " << x << std::endl;
            }
            return result;
        };
        EXPECT_TRUE(std::equal(received_values.begin(), received_values.end(), expected_values.begin(), std::move(near_equality_comparison)));
    }

    CovarianceMatrix ConstructCovarianceFromStdDev(const accerion::api::StandardDeviation& std_dev) const {
        CovarianceMatrix covar = {0};
        covar[0] = (std_dev.x * std_dev.x);
        covar[7] = (std_dev.y * std_dev.y);
        covar[35] =  pow(std_dev.th * accerion::api::kDegToRad, 2);
        return covar;
    }
    
    void CheckActiveModesFromDiagnosticsMsg(const std::string& active_field, const BitsTranslationMap& map)  const {
        for (const auto& map_it: map) {
            CheckExistanceOfStringInMsgField(active_field, map_it.second);
        }
    }

    void CheckExistanceOfStringInMsgField(const std::string& active_field, const std::string& map_string) const {
        EXPECT_TRUE(active_field.find(map_string) != std::string::npos)
                        << "'" << map_string << "' is missing from diagnostics msg";
    }

    template <typename T>
    T SetDiagnosticsCodes(const BitsTranslationMap& map) const {
        std::bitset<sizeof(T) * CHAR_BIT> bitset;
        for (const auto& map_it: map) {
            bitset.set(map_it.first);
        }
        return static_cast<T>(bitset.to_ulong());
    }

    template <typename SetCodeField, typename GetActiveField>
    void CheckDiagnosticsMsgCodesOneByOne(SetCodeField& set_codes_field, GetActiveField& get_active_field, const BitsTranslationMap& map) {
        accerion::api::Diagnostics diagnostics_msg{};
        for (const auto& map_it: map) {
            auto future_message = common_ros_test::GetFutureToExpectedMessage<accerion_driver_msgs::SensorDiagnostics>(pub_diagnostics_.getTopic());

            set_codes_field(diagnostics_msg, map_it.first);
            DiagnosticsAck(diagnostics_msg);
            const auto ros_diagnostics_msg = future_message.get();
            EXPECT_TRUE(ros_diagnostics_msg);

            EqualityCheckBetweenSNTimestampsCodesOfDiagnosticsMsgs(*ros_diagnostics_msg, diagnostics_msg);
            CheckExistanceOfStringInMsgField(get_active_field(*ros_diagnostics_msg), map_it.second);
        }
    }

    void EqualityCheckBetweenSNTimestampsCodesOfDiagnosticsMsgs(const accerion_driver_msgs::SensorDiagnostics& ros_diagnostics_msg, 
                                                                const accerion::api::Diagnostics& diag) const {
        EXPECT_EQ(ros_diagnostics_msg.raw_timestamp, diag.timestamp.time_since_epoch().count());
        EXPECT_EQ(ros_diagnostics_msg.serial_number, sensor_serial_);
        EXPECT_EQ(ros_diagnostics_msg.modes_code, diag.modes);
        EXPECT_EQ(ros_diagnostics_msg.warnings_code, diag.warning_codes);
        EXPECT_EQ(ros_diagnostics_msg.errors_code, diag.error_codes);
    }

    void NearEqualityCheckOfGeometryMsgPoint(const geometry_msgs::Point32& point, const std::array<double, 3>& expected_values) const {
        EXPECT_NEAR(point.x, expected_values[0], common_ros_test::kEps);
        EXPECT_NEAR(point.y, expected_values[1], common_ros_test::kEps);
        EXPECT_NEAR(point.z, expected_values[2], common_ros_test::kEps);
    }

    static constexpr accerion::api::Pose kTestPose{1, -2, 45};
    static constexpr accerion::api::StandardDeviation kTestStandardDeviation = {.x = 0.01,.y = 0.02,.th = 0.03};
};
