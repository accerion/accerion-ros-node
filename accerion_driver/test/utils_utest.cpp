/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "utils.h"

#include <gtest/gtest.h>

const float kAbsErrorFloat = 1e-6;
const double kAbsErrorDouble = 1e-14;

TEST(Utils, DegToRad) {
    int n_tests = 7;
    double input[n_tests] = {90., -90., 180., -180., 0., 360., -360.};
    double expected_result[n_tests] = {M_PI/2., -M_PI/2., M_PI, -M_PI, 0., 2.*M_PI, -2.*M_PI};
    for (int i = 0; i < n_tests; i++) {
        EXPECT_NEAR(expected_result[i], utils::deg2rad(input[i]), kAbsErrorDouble) 
        << "FAILED TO CONVERT DEG TO RAD FOR degrees = " << input[i] << "DEG (i = " << i << ")";
    }
}

TEST(Utils, RadToDeg) {
    int n_tests = 7;
    double input[n_tests] = {M_PI/2., -M_PI/2., M_PI, -M_PI, 0., 2.*M_PI, -2.*M_PI};
    double expected_result[n_tests] = {90., -90., 180., -180., 0., 360., -360.};
    for (int i = 0; i < n_tests; i++) {
        EXPECT_NEAR(expected_result[i], utils::rad2deg(input[i]), kAbsErrorDouble) 
        << "FAILED TO CONVERT RAD TO DEG FOR radians = " << input[i] << "RAD (i = " << i << ")";
    }
}

TEST(Utils, Rotate2dDouble) {
    int n_tests = 7; 
    double th[n_tests] = {M_PI/2., -M_PI/2., M_PI, -M_PI, 0., 2.*M_PI, -2.*M_PI};

    double expected_result[n_tests][2] = {
        {-4., 3.},
        {4., -3.},
        {-3., -4.},
        {-3., -4.},
        {3., 4.},
        {3., 4.},
        {3., 4.}
    };
    for (int i = 0; i < n_tests; i++) {
        double x = 3, y = 4;
        utils::rotate2d(x, y, th[i]);
        EXPECT_NEAR(expected_result[i][0], x, kAbsErrorDouble) 
        << "FAILED TO ROTATE x WITH TH = " << th[i] << "RAD (i = " << i << ")";
        EXPECT_NEAR(expected_result[i][1], y, kAbsErrorDouble) 
        << "FAILED TO ROTATE y WITH TH = " << th[i] << "RAD (i = " << i << ")";
    }    
}

TEST(Utils, Rotate2dFloat) {
    int n_tests = 7; 
    float th[n_tests] = {M_PI/2., -M_PI/2., M_PI, -M_PI, 0., 2.*M_PI, -2.*M_PI};

    float expected_result[n_tests][2] = {
        {-4., 3.},
        {4., -3.},
        {-3., -4.},
        {-3., -4.},
        {3., 4.},
        {3., 4.},
        {3., 4.}
    };
    for (int i = 0; i < n_tests; i++) {
        float x = 3, y = 4;
        utils::rotate2d(x, y, th[i]);
        EXPECT_NEAR(expected_result[i][0], x, kAbsErrorFloat) 
        << "FAILED TO ROTATE x WITH th = " << th[i] << "RAD (i = " << i << ")";
        EXPECT_NEAR(expected_result[i][1], y, kAbsErrorFloat) 
        << "FAILED TO ROTATE y WITH th = " << th[i] << "RAD (i = " << i << ")";
    }    
  }

TEST(Utils, GetAngularDiff) {
    int n_tests = 5;
    double inputs[n_tests][2] = {
        {-M_PI/2., M_PI/2.},
        {M_PI/2., -M_PI/2.},
        {M_PI/2., M_PI/2.},
        {M_PI, -M_PI/2.},
        {-M_PI, M_PI/2.}
    };
    double expected_result[n_tests] = {-M_PI, M_PI, 0, -M_PI/2., M_PI/2.};

    for (int i = 0; i < n_tests; i++) {
        EXPECT_NEAR(expected_result[i], utils::getAngularDiff(inputs[i][0], inputs[i][1]), kAbsErrorDouble) 
        << "FAILED TO SUBTRACT " << inputs[i][0] << "RAD FROM " << inputs[i][1] << "RAD CORRECTLY (i = " << i << ")";
    }
}

TEST(Utils, GetAngularSum) {
    int n_tests = 5;
    double inputs[n_tests][2] = {
        {M_PI/2., M_PI/2.},
        {-M_PI/2., -M_PI/2.},
        {-M_PI/2., M_PI/2.},
        {M_PI, M_PI/2.},
        {-M_PI, -M_PI/2.}
    };
    double expected_result[n_tests] = {M_PI, -M_PI, 0, -M_PI/2., M_PI/2.};
    
    for (int i = 0; i < n_tests; i++) {
        EXPECT_NEAR(expected_result[i], utils::getAngularSum(inputs[i][0], inputs[i][1]), kAbsErrorDouble) 
        << "FAILED TO SUM " << inputs[i][0] << "RAD AND " << inputs[i][1] << "RAD CORECTLY (i = " << i << ")";
    }
}

TEST(Utils, CorrectRadRangeFloat) {
    int n_tests = 3;
    float th[n_tests] = {M_PI/2., 3.*M_PI, -3.*M_PI};
    float expected_result[n_tests] = {M_PI/2., M_PI, -M_PI};
    for (int i = 0; i< n_tests; i++) {
        utils::correctRadRange(th[i]);
        EXPECT_NEAR(expected_result[i], th[i], kAbsErrorFloat) 
        << "FAILED TO CONVERT " << th[i] << "RAD TO CORRECT RANGE (i = " << i << ")";
    }
}

TEST(Utils, CorrectRadRangeDouble) {
    int n_tests = 3;
    double th[n_tests] = {M_PI/2., 3.*M_PI, -3.*M_PI};
    double expected_result[n_tests] = {M_PI/2., M_PI, -M_PI};
    for (int i = 0; i< n_tests; i++) {
        utils::correctRadRange(th[i]);
        EXPECT_NEAR(expected_result[i], th[i], kAbsErrorDouble) 
        << "FAILED TO CONVERT " << th[i] << "RAD TO CORRECT RANGE (i = " << i << ")";
    }
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}