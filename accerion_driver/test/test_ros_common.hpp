/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <ros/topic_manager.h>

#include <algorithm>
#include <gtest/gtest.h>

#include <future>


namespace common_ros_test {
    static constexpr auto kEps = 1.e-6;
    
    inline bool CheckSubscribing(const std::vector<ros::Publisher> &publishers) {
        bool is_subscribing = true;
        for (const auto& publisher: publishers)
            is_subscribing = (is_subscribing && publisher.getNumSubscribers() > 0);

        return is_subscribing;
    }

    inline bool CheckAdvertising(const std::vector<ros::Subscriber> &subscribers) {
        bool is_advertising = true;
        for (const auto& subscriber: subscribers)
            is_advertising = (is_advertising && subscriber.getNumPublishers() > 0);

        return is_advertising;
    }

    inline bool IsAlive(std::string node_name) {
        std::vector<std::string> nodes;
        ros::master::getNodes(nodes);

        if (std::find(nodes.begin(), nodes.end(), node_name) != nodes.end()) {
            return true;
        }
        return false;
    }

    inline void WaitUntilAlive(std::string node_name) {
        while (!IsAlive(node_name))
            ros::Duration(0.1).sleep();
    }

    inline tf::StampedTransform LookupTransform(tf::TransformListener& tf_listener, const std::string &target_frame, const std::string &source_frame) {
        tf::StampedTransform target_to_source_tf;

        try {

            tf_listener.waitForTransform(target_frame, source_frame, ros::Time(0), ros::Duration(2.0));
            tf_listener.lookupTransform(target_frame, source_frame, ros::Time(0), target_to_source_tf);

        } catch (...) {
            std::cout << "RosTest: lookupTransform error! Does the tf from " << target_frame << " to " << source_frame << " exist?" << std::endl;
        }

        return target_to_source_tf;
    }

    inline void CheckStampedTransform(const tf::StampedTransform &tf, const std::vector<double> &expected_values) {
        auto yaw = tf::getYaw(tf.getRotation());

        EXPECT_NEAR(tf.getOrigin().x(), expected_values[0], kEps) << "X VALUE FOR TRANSFORM FROM " << tf.frame_id_ << " TO " << tf.child_frame_id_ << " DIFFERS." << std::endl;
        EXPECT_NEAR(tf.getOrigin().y(), expected_values[1], kEps) << "Y VALUE FOR TRANSFORM FROM " << tf.frame_id_ << " TO " << tf.child_frame_id_ << " DIFFERS." << std::endl;
        EXPECT_NEAR(yaw*180./M_PI, expected_values[2], kEps) << "YAW ANGLE FOR TRANSFORM FROM " << tf.frame_id_ << " TO " << tf.child_frame_id_ << " DIFFERS." << std::endl;
    }

    template <typename MessageType>
    std::future<boost::shared_ptr<const MessageType>> GetFutureToExpectedMessage(const std::string& topic, const float seconds_to_wait_for = 2.0) {
        auto topic_manager = ros::TopicManager::instance();
        const auto n_subscribers = topic_manager->getNumSubscribers(topic);
        auto future_message = std::async(std::launch::async, [&topic, seconds_to_wait_for](){
            return ros::topic::waitForMessage<MessageType>(topic, ros::Duration(seconds_to_wait_for)); });

        while((!topic_manager->getNumSubscribers(topic) > n_subscribers) &&
            (future_message.wait_for(std::chrono::milliseconds{100}) != std::future_status::ready));
            
        return future_message;
    }
}