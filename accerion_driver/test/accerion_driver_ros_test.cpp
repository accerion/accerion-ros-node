/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <gtest/gtest.h>

#include "accerion_driver_ros_test.hpp"

#include "AccerionSensorAPI/structs/pose.h"
#include "test_ros_common.hpp"


TEST_F(AccerionDriverTest, TestCorrectedPoseAck) {
    auto future_message = common_ros_test::GetFutureToExpectedMessage<nav_msgs::Odometry>(pub_corrected_odom_.getTopic());

    accerion::api::CorrectedOdometry corrected_odom_msg{};
    corrected_odom_msg.pose = kTestPose; 
    corrected_odom_msg.standard_deviation = {kTestStandardDeviation.x/10., kTestStandardDeviation.y/10., kTestStandardDeviation.th/10.};

    CorrectedOdometryAck(corrected_odom_msg);
    const auto ros_global_odom_msg = future_message.get();

    EXPECT_TRUE(ros_global_odom_msg);
    EqualityCheckBetweenPoses(ros_global_odom_msg->pose.pose, corrected_odom_msg.pose);
    const CovarianceMatrix expected_covar = ConstructCovarianceFromStdDev(corrected_odom_msg.standard_deviation);
    NearEqualityCheckOfCovarianceMatrix(ros_global_odom_msg->pose.covariance, expected_covar);
}

TEST_F(AccerionDriverTest, TestUncorrectedPoseAck) {
    auto future_message = common_ros_test::GetFutureToExpectedMessage<nav_msgs::Odometry>(pub_odom_.getTopic());

    accerion::api::Odometry odometry{};
    odometry.pose = kTestPose; 
    odometry.velocity.x = 1.5; 
    odometry.velocity.y = 2.; 
    odometry.velocity.th = 0.3;
    odometry.standard_deviation_velocity = kTestStandardDeviation;

    odometry.pose = kTestPose;
    OdometryAck(odometry);
    const auto ros_local_odom_msg = future_message.get();

    EXPECT_TRUE(ros_local_odom_msg);
    EqualityCheckBetweenPoses(ros_local_odom_msg->pose.pose, odometry.pose);
    EqualityCheckBetweenVelocities(
        ros_local_odom_msg->twist.twist,
        {odometry.velocity.x, odometry.velocity.y, odometry.velocity.th * accerion::api::kDegToRad});
    const CovarianceMatrix expected_covar_velocity = ConstructCovarianceFromStdDev(odometry.standard_deviation_velocity);
    NearEqualityCheckOfCovarianceMatrix(ros_local_odom_msg->twist.covariance, expected_covar_velocity);
}

TEST_F(AccerionDriverTest, TestDiagnosticsAck) {
    auto future_message = common_ros_test::GetFutureToExpectedMessage<accerion_driver_msgs::SensorDiagnostics>(pub_diagnostics_.getTopic());

    accerion::api::Diagnostics diagnostics_msg{};
    diagnostics_msg.timestamp = accerion::api::TimePoint{std::chrono::nanoseconds{1111111111}};

    diagnostics_msg.modes = SetDiagnosticsCodes<decltype(accerion::api::Diagnostics::modes)>(accerion::api::kModeDescriptionMap);
    diagnostics_msg.warning_codes = SetDiagnosticsCodes<decltype(accerion::api::Diagnostics::warning_codes)>(accerion::api::kWarningMap);
    diagnostics_msg.error_codes = SetDiagnosticsCodes<decltype(accerion::api::Diagnostics::error_codes)>(accerion::api::kErrorMap);
    sensor_serial_ = 598101179;

    DiagnosticsAck(diagnostics_msg);
    const auto ros_diagnostics_msg = future_message.get();
    EXPECT_TRUE(ros_diagnostics_msg);

    EqualityCheckBetweenSNTimestampsCodesOfDiagnosticsMsgs(*ros_diagnostics_msg, diagnostics_msg);

    CheckActiveModesFromDiagnosticsMsg(ros_diagnostics_msg->active_modes, accerion::api::kModeDescriptionMap);
    CheckActiveModesFromDiagnosticsMsg(ros_diagnostics_msg->active_warnings, accerion::api::kWarningMap);
    CheckActiveModesFromDiagnosticsMsg(ros_diagnostics_msg->active_errors, accerion::api::kErrorMap);
}

TEST_F(AccerionDriverTest, TestDiagnosticsModesOneByOne) {
    auto SetMode = [](accerion::api::Diagnostics& diagnostics_msg, int map_key) {
        diagnostics_msg.modes = 1 << map_key; 
    };
    auto GetActiveModesField = [](const accerion_driver_msgs::SensorDiagnostics& ros_diagnostics_msg) {
        return ros_diagnostics_msg.active_modes; 
    };
    CheckDiagnosticsMsgCodesOneByOne(SetMode, GetActiveModesField, accerion::api::kModeDescriptionMap);
}

TEST_F(AccerionDriverTest, TestDiagnosticsWarningOneByOne) {
    auto SetWarningCode = [](accerion::api::Diagnostics& diagnostics_msg, int map_key) {
        diagnostics_msg.warning_codes = 1 << map_key; 
    };
    auto GetActiveWarningsField = [](const accerion_driver_msgs::SensorDiagnostics& ros_diagnostics_msg) {
        return ros_diagnostics_msg.active_warnings; 
    };
    CheckDiagnosticsMsgCodesOneByOne(SetWarningCode, GetActiveWarningsField, accerion::api::kWarningMap);
}

TEST_F(AccerionDriverTest, TestDiagnosticsErrorsOneByOne) {
    auto SetErrorCode = [](accerion::api::Diagnostics& diagnostics_msg, int map_key) {
        diagnostics_msg.error_codes = 1 << map_key; 
    };
    auto GetActiveErrorsField = [](const accerion_driver_msgs::SensorDiagnostics& ros_diagnostics_msg) {
        return ros_diagnostics_msg.active_errors; 
    };
    CheckDiagnosticsMsgCodesOneByOne(SetErrorCode, GetActiveErrorsField, accerion::api::kErrorMap);
}

TEST_F(AccerionDriverTest, TestDriftCorrectionAck) {
    auto future_message_global_corrections = common_ros_test::GetFutureToExpectedMessage<nav_msgs::Odometry>(pub_global_corrections_.getTopic());
    auto future_message_drift_details = common_ros_test::GetFutureToExpectedMessage<accerion_driver_msgs::DriftCorrectionDetails>(pub_drift_details_.getTopic());

    accerion::api::DriftCorrection drift_corrections_msg{};
    drift_corrections_msg.pose = kTestPose;
    drift_corrections_msg.delta.x = kTestPose.x/10.;
    drift_corrections_msg.delta.y = kTestPose.y/10.;
    drift_corrections_msg.delta.th = kTestPose.th/10.;
    drift_corrections_msg.cumulative_travelled_distance = 20.;
    drift_corrections_msg.cumulative_travelled_heading = 1963;
    drift_corrections_msg.error_percentage = 0.08;
    drift_corrections_msg.cluster_id = 15;
    drift_corrections_msg.quality_estimate = 1;
    drift_corrections_msg.standard_deviation = kTestStandardDeviation;

    DriftCorrectionAck(drift_corrections_msg);
    const auto ros_global_corrections_msg = future_message_global_corrections.get();
    const auto ros_drift_details_msg = future_message_drift_details.get();

    EXPECT_TRUE(ros_global_corrections_msg);
    EXPECT_STREQ(ros_global_corrections_msg->header.frame_id.c_str(), global_parent_frame_.c_str());
    EXPECT_STREQ(ros_global_corrections_msg->child_frame_id.c_str(), sensor_link_.c_str());
    EqualityCheckBetweenPoses(ros_global_corrections_msg->pose.pose, drift_corrections_msg.pose);
    const CovarianceMatrix expected_covar = ConstructCovarianceFromStdDev(drift_corrections_msg.standard_deviation);
    NearEqualityCheckOfCovarianceMatrix(ros_global_corrections_msg->pose.covariance, expected_covar);
    EXPECT_EQ(ros_global_corrections_msg->header.seq, correction_counter_-1);
    
    EXPECT_TRUE(ros_drift_details_msg);
    EqualityCheckBetweenPoses(ros_drift_details_msg->pose, drift_corrections_msg.pose);
    EqualityCheckBetweenPoses(ros_drift_details_msg->error, drift_corrections_msg.delta);
    EXPECT_FLOAT_EQ(ros_drift_details_msg->cumulative_traveled_distance, drift_corrections_msg.cumulative_travelled_distance);
    EXPECT_FLOAT_EQ(ros_drift_details_msg->cumulative_traveled_rotation, drift_corrections_msg.cumulative_travelled_heading * accerion::api::kDegToRad);
    EXPECT_FLOAT_EQ(ros_drift_details_msg->error_percentage, drift_corrections_msg.error_percentage);
    EXPECT_EQ(ros_drift_details_msg->cluster_id, drift_corrections_msg.cluster_id);
    EXPECT_EQ(ros_drift_details_msg->quality_estimate, drift_corrections_msg.quality_estimate);
}

TEST_F(AccerionDriverTest, TestLineFollowerAck) {
    auto future_message_line_follower_visualization = common_ros_test::GetFutureToExpectedMessage<geometry_msgs::PolygonStamped>(pub_line_follower_visualization_.getTopic());
    auto future_message_line_follower = common_ros_test::GetFutureToExpectedMessage<geometry_msgs::PoseArray>(pub_line_follower_.getTopic());

    accerion::api::LineFollowerData line_follower_data_msg{};
    line_follower_data_msg.pose = kTestPose;
    line_follower_data_msg.closest_point.x = kTestPose.x + 1.536;
    line_follower_data_msg.closest_point.y = kTestPose.y - 0.536;
    line_follower_data_msg.closest_point.th = kTestPose.th + 10.536;
    line_follower_data_msg.cluster_id = 15;

    LineFollowerAck(line_follower_data_msg);
    const auto ros_line_follower_visualization_msg = future_message_line_follower_visualization.get();
    const auto ros_line_follower_msg = future_message_line_follower.get();

    EXPECT_TRUE(ros_line_follower_visualization_msg);
    EXPECT_STREQ(ros_line_follower_visualization_msg->header.frame_id.c_str(), global_parent_frame_.c_str());
    auto polygon_length = ros_line_follower_visualization_msg->polygon.points.size();
    EXPECT_EQ(polygon_length, 2) << "Expected 2 points in line_follower visualization msgs but got " << polygon_length << std::endl;
    NearEqualityCheckOfGeometryMsgPoint(
        ros_line_follower_visualization_msg->polygon.points[0],
        std::array<double, 3>{line_follower_data_msg.pose.x,
                              line_follower_data_msg.pose.y, 0.0});
    NearEqualityCheckOfGeometryMsgPoint(
        ros_line_follower_visualization_msg->polygon.points[1],
        std::array<double, 3>{line_follower_data_msg.closest_point.x,
                              line_follower_data_msg.closest_point.y, 0.0});

    EXPECT_TRUE(ros_line_follower_msg);
    EXPECT_STREQ(ros_line_follower_msg->header.frame_id.c_str(), global_parent_frame_.c_str());
    auto poses_length = ros_line_follower_msg->poses.size();
    EXPECT_EQ(poses_length, 2) << "Expected 2 poses in line_follower msgs but got " << poses_length << std::endl;
    EqualityCheckBetweenPoses(ros_line_follower_msg->poses[0], line_follower_data_msg.pose);
    EqualityCheckBetweenPoses(ros_line_follower_msg->poses[1],
                              {line_follower_data_msg.closest_point.x,
                               line_follower_data_msg.closest_point.y,
                               line_follower_data_msg.closest_point.th});
}

TEST_F(AccerionDriverTest, TestSignatureMarkerAck) {
    auto future_message = common_ros_test::GetFutureToExpectedMessage<sensor_msgs::PointCloud2>(pub_map_as_point_cloud2_.getTopic());
    SignatureMarkerAck(accerion::api::SignatureVector());
    auto map_as_point_cloud2 = future_message.get();
    EXPECT_TRUE(map_as_point_cloud2);
}

TEST_F(AccerionDriverTest, TestArucoMarkerAck) {
    auto future_message = common_ros_test::GetFutureToExpectedMessage<visualization_msgs::MarkerArray>(pub_marker_visualization_.getTopic());
    auto marker = accerion::api::MarkerDetection{   .timestamp = accerion::api::TimePoint{std::chrono::nanoseconds{0}},
                                                    .type = accerion::api::MarkerType::kAruco,
                                                    .id = "1",
                                                    .pose = {0.0, 0.0, 0.0}};
    MarkerAck(marker);
    const auto aruco_visualization = future_message.get();

    EXPECT_TRUE(aruco_visualization);
}

TEST_F(AccerionDriverTest, CreatedSignatureAck) {
    auto future_message = common_ros_test::GetFutureToExpectedMessage<accerion_driver_msgs::CreatedSignature>(pub_created_signatures_.getTopic());

    accerion::api::CreatedSignature created_signature;
    created_signature.signature.signature_id = 10;
    created_signature.signature.cluster_id = 0;
    created_signature.signature.pose = kTestPose;

    CreatedSignatureAck(created_signature);
    const auto created_signature_message = future_message.get();

    EXPECT_TRUE(created_signature_message);
    EqualityCheckBetweenPoses(created_signature_message->pose,
                              created_signature.signature.pose);
    EXPECT_EQ(created_signature.signature.signature_id, created_signature_message->signature_id);
    EXPECT_EQ(created_signature.signature.cluster_id, created_signature_message->cluster_id);
}

// Run all the tests that were declared with TEST_F()
int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "accerion_driver_tester");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    int ret = RUN_ALL_TESTS();
    spinner.stop();
    return ret;
}
