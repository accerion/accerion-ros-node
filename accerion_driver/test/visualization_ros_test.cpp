/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Odometry.h>
#include <rosgraph_msgs/Log.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PolygonStamped.h>

#include "test_ros_common.hpp"

#include <gtest/gtest.h>
#include <algorithm>

class VisualizationTest : public ::testing::Test {
public:
    VisualizationTest() {
        visualization_node_name_ = nh_.param<std::string>("visualization_node_name", "accerion_visualization");

        sub_output_velocity_ = nh_.subscribe("velocity", 10, &VisualizationTest::CallbackVelocity, this);
        sub_output_lin_velocity_ = nh_.subscribe("lin_velocity", 10, &VisualizationTest::CallbackLinVelocity, this);
        sub_output_ang_velocity_ = nh_.subscribe("ang_velocity", 10, &VisualizationTest::CallbackAngVelocity, this);
        sub_console_output_ = nh_.subscribe("driver_output", 100, &VisualizationTest::CallbackConsoleOutput, this);
        sub_search_ranges_ = nh_.subscribe("search_ranges", 10, &VisualizationTest::CallbackSearchRange, this);

        pub_uncorrected_odometry_ = nh_.advertise <nav_msgs::Odometry> ("odom", 1);
        pub_corrected_pose_ = nh_.advertise <nav_msgs::Odometry> ("corrected_odom", 1);
        pub_rosout_agg_ = nh_.advertise <rosgraph_msgs::Log> ("/rosout_agg", 100);

        common_ros_test::WaitUntilAlive(visualization_node_name_);

        std::cout << "visualization_tester node initialized" << std::endl;
        ros::Duration(1.0).sleep();
    }

    void PubUncorrectedOdometry(nav_msgs::Odometry msg) const {
        pub_uncorrected_odometry_.publish(msg);
        ros::Duration(1.0).sleep();
    }

    void PubCorrectedPose(nav_msgs::Odometry msg) const {
        pub_corrected_pose_.publish(msg);
        ros::Duration(1.0).sleep();
    }

    void PubRosoutAgg(rosgraph_msgs::Log msg) const {
        pub_rosout_agg_.publish(msg);
        ros::Duration(1.0).sleep();
    }

protected:
    void CallbackVelocity(const geometry_msgs::TwistStamped& msg) {
        velocity_msg_ = msg;
        velocity_msg_received_ = true;
    }

    void CallbackLinVelocity(const std_msgs::Float32& msg) {
        lin_velocity_msg_ = msg;
        lin_velocity_msg_received_ = true;
    }

    void CallbackAngVelocity(const std_msgs::Float32& msg) {
        ang_velocity_msg_ = msg;
        ang_velocity_msg_received_ = true;
    }

    void CallbackConsoleOutput(const visualization_msgs::MarkerArray& msg) {
        marker_array_msg_received_ = true;
    }

    void CallbackSearchRange(const geometry_msgs::PolygonStamped& msg) {
        polygon_stamped_msg_ = msg;
        polygon_stamped_msg_received_ = true;
    }

    ros::NodeHandle nh_;
    ros::Publisher pub_uncorrected_odometry_, pub_corrected_pose_, pub_rosout_agg_;
    ros::Subscriber sub_output_velocity_, sub_output_lin_velocity_;
    ros::Subscriber sub_output_ang_velocity_, sub_console_output_, sub_search_ranges_;
    
    std::string visualization_node_name_;

    bool velocity_msg_received_{false}, lin_velocity_msg_received_{false}, ang_velocity_msg_received_{false}, marker_array_msg_received_{false}, polygon_stamped_msg_received_{false};
    geometry_msgs::TwistStamped velocity_msg_;
    std_msgs::Float32 lin_velocity_msg_, ang_velocity_msg_;
    geometry_msgs::PolygonStamped polygon_stamped_msg_;
};

TEST_F(VisualizationTest, CheckSubscribing) {
    bool is_subscribing = common_ros_test::CheckSubscribing({pub_uncorrected_odometry_, pub_corrected_pose_, pub_rosout_agg_});

    EXPECT_TRUE(is_subscribing);
}

TEST_F(VisualizationTest, CheckAdvertising) {       
    bool is_advertising = common_ros_test::CheckAdvertising(
            {sub_output_velocity_, sub_output_lin_velocity_, sub_output_ang_velocity_, sub_console_output_, sub_search_ranges_});

    EXPECT_TRUE(is_advertising);
}

TEST_F(VisualizationTest, TestVelocityPub) {
    nav_msgs::Odometry msg;
    ros::Time timestamp = ros::Time::now();
    msg.header.stamp = timestamp;
    msg.child_frame_id = "test";
    msg.twist.twist.linear.x = 0.5;
    msg.twist.twist.linear.y = 0.1;
    msg.twist.twist.angular.z = 1;

    PubUncorrectedOdometry(msg);

    EXPECT_TRUE(velocity_msg_received_);
    EXPECT_TRUE(lin_velocity_msg_received_);
    EXPECT_TRUE(ang_velocity_msg_received_);

    EXPECT_FLOAT_EQ(lin_velocity_msg_.data, std::sqrt((msg.twist.twist.linear.x*msg.twist.twist.linear.x) + (msg.twist.twist.linear.y*msg.twist.twist.linear.y)));
}

TEST_F(VisualizationTest, TestPolygonStampedPub) {
    nav_msgs::Odometry msg;
    msg.pose.covariance[0] = 1;
    msg.pose.covariance[7] = 2;
    msg.pose.covariance[35] = 1;

    PubCorrectedPose(msg);

    EXPECT_TRUE(polygon_stamped_msg_received_);
    EXPECT_FLOAT_EQ(polygon_stamped_msg_.polygon.points[0].x, 3*std::sqrt(msg.pose.covariance[7]));

}

TEST_F(VisualizationTest, TestMarkerArrayPub) {
    rosgraph_msgs::Log msg;
    msg.name = "/accerion_driver";

    PubRosoutAgg(msg);

    EXPECT_TRUE(marker_array_msg_received_);
}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "visualization_tester");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    int ret = RUN_ALL_TESTS();
    spinner.stop();
    return ret;
}