#!/usr/bin/env python3

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    # #  don't do this unless you want a globally visible script
    # scripts=["bin/myscript"],
    packages=['python_visualization', 'g2o'],
    package_dir={'python_visualization': 'accerionsensorapi/tools/python_visualization',
                 'g2o': 'accerionsensorapi/tools/python_visualization/g2o'}
)

setup(**d)
