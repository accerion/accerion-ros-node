/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <arpa/inet.h>
#include <string>

#include <ros/ros.h>

#include <std_msgs/Bool.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PoseArray.h>
#include <visualization_msgs/MarkerArray.h>

#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>

#include <accerion_driver_msgs/SensorDiagnostics.h>
#include <accerion_driver_msgs/DriftCorrectionDetails.h>
#include <accerion_driver_msgs/EmptyCommand.h>
#include <accerion_driver_msgs/Event.h>
#include <accerion_driver_msgs/SetBufferedRecoveryBufferLength.h>
#include <accerion_driver_msgs/StartBufferedRecoveryMode.h>
#include <accerion_driver_msgs/Cluster.h>
#include <accerion_driver_msgs/ModeCommand.h>
#include <accerion_driver_msgs/ModeClusterCommand.h>
#include <accerion_driver_msgs/DeleteArea.h>
#include <accerion_driver_msgs/RequestCommand.h>
#include <accerion_driver_msgs/ParentChildFrames.h>
#include <accerion_driver_msgs/Pose.h>
#include <accerion_driver_msgs/UDPSettings.h>
#include <accerion_driver_msgs/RecordingsList.h>
#include <accerion_driver_msgs/CreatedSignature.h>
#include <accerion_driver_msgs/SetIP.h>
#include <accerion_driver_msgs/DateTime.h>
#include <accerion_driver_msgs/GetAreas.h>
#include <accerion_driver_msgs/SetArea.h>
#include <accerion_driver_msgs/RamRomStatsSRV.h>
#include <accerion_driver_msgs/SearchRange.h>
#include <std_srvs/SetBool.h>
#include <std_srvs/Empty.h>

#include "AccerionSensorAPI/structs/created_signature.h"
#include "AccerionSearchLoopClosuresServer.h"
#include "AccerionGetMapServer.h"
#include "AccerionSendMapServer.h"
#include "AccerionSubsetServer.h"
#include "AccerionGetLogsServer.h"
#include "AccerionRecordingsServer.h"
#include "AccerionUpdateSensorServer.h"

class AccerionDriverROS {
 public:
  AccerionDriverROS();
  void LoadParameters();
  void InitPublishersAndSubscribers();
  void InitServices();
  void InitSignatureMarker();

  int run();

  /**
   * @brief Get the Selected Connection Type based on user input
   * 
   * @param connection_type the connection type requested by the user
   * @return accerion::api::ConnectionType 
   */
  accerion::api::ConnectionType GetSelectedConnectionType() const;
  void ConnectToSensor();
  void SubscribeToSensorMsgs();
  void InitActions();

  uint32_t sensor_serial_;

  std::string connection_type_;

 protected:
  void CorrectedOdometryAck(accerion::api::CorrectedOdometry co);

  void OdometryAck(accerion::api::Odometry up);

  void DiagnosticsAck(accerion::api::Diagnostics diag);

  void DriftCorrectionAck(accerion::api::DriftCorrection dc);

  void LineFollowerAck(accerion::api::LineFollowerData lfd);

  void SignatureMarkerAck(accerion::api::SignatureVector vector);

  void SignatureMarkerStartStopAck(accerion::api::Acknowledgement ack);

  void MarkerAck(accerion::api::MarkerDetection marker) const;

  void BufferedRecoveryModeProgressIndicator(accerion::api::BufferProgress buffer_progress) const;

  void IpAddressAck(accerion::api::IpAddress ip) const;

  void CreatedSignatureAck(accerion::api::CreatedSignature ns);
  void VisualizeSignatures(const accerion::api::SignatureVector& vec);

  void CallbackExternalReferencePose(const geometry_msgs::PoseStamped::ConstPtr &ext_ref_msg);
  void CallbackCorrectionSearchRange(const accerion_driver_msgs::SearchRange& sr);

  bool CallbackServiceBufferedRecoveryModeSetBufferLength(accerion_driver_msgs::SetBufferedRecoveryBufferLength::Request &req,
                                                          accerion_driver_msgs::SetBufferedRecoveryBufferLength::Response &res);

  bool CallbackServiceBufferedRecoveryModeGetBufferLength(accerion_driver_msgs::EmptyCommand::Request &req,
                                                          accerion_driver_msgs::EmptyCommand::Response &res);

  bool CallbackServiceStartBufferedRecoveryMode(accerion_driver_msgs::StartBufferedRecoveryMode::Request &req,
                                                accerion_driver_msgs::StartBufferedRecoveryMode::Response &res);

  bool CallbackServiceStopBufferedRecoveryMode(accerion_driver_msgs::EmptyCommand::Request &req,
                                               accerion_driver_msgs::EmptyCommand::Response &res);

  bool CallbackServiceDeleteCluster(accerion_driver_msgs::Cluster::Request &req,
                                    accerion_driver_msgs::Cluster::Response &res);

  bool CallbackServiceDeleteArea(accerion_driver_msgs::DeleteArea::Request &req,
                                 accerion_driver_msgs::DeleteArea::Response &res);

  bool CallbackServiceMode(accerion_driver_msgs::ModeCommand::Request &req,
                           accerion_driver_msgs::ModeCommand::Response &res);
  bool CallbackServiceModeWithCluster(accerion_driver_msgs::ModeClusterCommand::Request &req,
                                      accerion_driver_msgs::ModeClusterCommand::Response &res);
  bool CallbackServiceRequest(accerion_driver_msgs::RequestCommand::Request &req,
                              accerion_driver_msgs::RequestCommand::Response &res);
  bool CallbackServiceSetCorrectedOdometry(accerion_driver_msgs::Pose::Request &req,
                              accerion_driver_msgs::Pose::Response &res);
  bool CallbackServiceSetInternalMode(std_srvs::SetBool::Request &req,
                                          std_srvs::SetBool::Response &res);
  bool CallbackServiceSetUDPSettings(accerion_driver_msgs::UDPSettings::Request &req,
                                     accerion_driver_msgs::UDPSettings::Response &res);
  bool CallbackServiceRecordings(accerion_driver_msgs::RecordingsList::Request &req,
                                 accerion_driver_msgs::RecordingsList::Response &res);
  bool CallbackServiceSetIPAddress(accerion_driver_msgs::SetIP::Request &req,
                                   accerion_driver_msgs::SetIP::Response &res);
  bool CallbackServiceSetDateTime(accerion_driver_msgs::DateTime::Request &req,
                                  accerion_driver_msgs::DateTime::Response &res);
  bool CallbackServiceGetAreas(accerion_driver_msgs::GetAreas::Request &req,
                               accerion_driver_msgs::GetAreas::Response &res);
  bool CallbackServiceSetArea(accerion_driver_msgs::SetArea::Request &req,
                              accerion_driver_msgs::SetArea::Response &res);
  bool CallbackServiceGetRamRomStats(
      accerion_driver_msgs::RamRomStatsSRV::Request &req,
      accerion_driver_msgs::RamRomStatsSRV::Response &res);
  bool CallbackServiceLogEvent(accerion_driver_msgs::Event::Request &req,
                                    accerion_driver_msgs::Event::Response &res);
  bool CallbackServiceSetSaveImages(std_srvs::SetBool::Request &req,
                                      std_srvs::SetBool::Response &res);

  ros::Time LatencyCompensatedTimestamp() const;

  /*Comm related*/
  std::shared_ptr<accerion::api::Sensor> accerion_sensor_ = nullptr;

  ros::NodeHandle nh_;
  std::unique_ptr<ros::NodeHandle> pnh_;
  std::string node_ns_;

  ros::Publisher pub_corrected_odom_, pub_odom_, pub_global_corrections_;
  ros::Publisher pub_drift_details_;
  ros::Publisher pub_diagnostics_;
  ros::Publisher pub_map_as_point_cloud2_;
  ros::Publisher pub_created_signatures_;
  ros::Publisher pub_line_follower_visualization_;
  ros::Publisher pub_line_follower_;
  ros::Publisher pub_marker_visualization_;
  ros::Subscriber subs_external_odometry_;
  ros::Subscriber sub_search_range_;

  ros::ServiceServer service_server_mode_;
  ros::ServiceServer service_server_mode_with_cluster_;
  ros::ServiceServer service_server_request_;

  ros::ServiceServer service_server_buffered_recovery_mode_set_buffer_length__;
  ros::ServiceServer service_server_buffered_recovery_mode_get_buffer_length__;
  ros::ServiceServer service_server_buffered_recovery_mode_start_;
  ros::ServiceServer service_server_buffered_recovery_mode_stop_;
  ros::ServiceServer service_server_delete_area_;
  ros::ServiceServer service_server_delete_cluster_;
  ros::ServiceServer service_server_remove_area_;
  ros::ServiceServer service_server_set_corrected_odometry_;
  ros::ServiceServer service_server_set_internal_mode_;
  ros::ServiceServer service_server_set_udp_settings_;
  ros::ServiceServer service_server_log_event_;

  ros::ServiceServer service_server_save_images_;

  ros::ServiceServer service_server_recordings_;

  ros::ServiceServer service_server_set_ip_;
  ros::ServiceServer service_server_set_date_time_;
  ros::ServiceServer service_server_get_areas_;
  ros::ServiceServer service_server_set_area_;
  ros::ServiceServer service_server_get_ram_rom_stats_;

  std::unique_ptr<AccerionSearchLoopClosuresServer> accerion_search_loop_closures_server_ = nullptr;
  std::unique_ptr<AccerionGetMapServer> accerion_getMap_server_ = nullptr;
  std::unique_ptr<AccerionSendMapServer> accerion_sendMap_server_ = nullptr;
  std::unique_ptr<AccerionSubsetServer> accerion_subset_server_ = nullptr;
  std::unique_ptr<AccerionGetLogsServer> accerion_get_logs_server_ = nullptr;
  std::unique_ptr<AccerionRecordingsServer> accerion_recordings_server_ = nullptr;
  std::unique_ptr<AccerionUpdateSensorServer> accerion_update_sensor_server_ = nullptr;

  std::string global_parent_frame_, local_parent_frame_, sensor_link_;
  std::string ext_ref_pose_topic_;
  std::string search_range_topic_;
  std::string local_ip_;
  std::string default_local_ip_ = "0.0.0.0";

  double ext_ref_pose_latency_; // fixed latency options in ms, if unset (default is 0)
  double output_timestamp_compensation_; // fixed latency option in ms, default is 20
  bool ext_ref_timestamp_compensation_; // calculate latency from ros timestamps

  std::size_t correction_counter_;

  tf::TransformListener tf_listener_;

  sensor_msgs::PointCloud2 map_point_cloud2_;
  std::unique_ptr<sensor_msgs::PointCloud2Modifier> point_cloud2_modifier_;
};

