/* Copyright (c) 2017-2021, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef ACCERIONGETMAPSERVERROS_H
#define ACCERIONGETMAPSERVERROS_H

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <accerion_driver_msgs/GetMapAction.h>

#include "AccerionSensorAPI/sensor.h"

class AccerionGetMapServer {
 protected:
  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<accerion_driver_msgs::GetMapAction>
      as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
  std::shared_ptr<accerion::api::Sensor> sensor_ = nullptr;

 public:
  AccerionGetMapServer(std::string name, std::shared_ptr<accerion::api::Sensor> sensor);
  void ExecuteCallback(const accerion_driver_msgs::GetMapGoalConstPtr &goal);
};
#endif // ACCERIONGETMAPSERVERROS_H