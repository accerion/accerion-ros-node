/* Copyright (c) 2017-2021, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef ACCERIONUPDATESENSORERVERROS_H
#define ACCERIONUPDATESENSORERVERROS_H

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <accerion_driver_msgs/UpdateSensorAction.h>
#include "AccerionSensorAPI/utils/sensor.h"

class AccerionUpdateSensorServer {
 protected:
  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<accerion_driver_msgs::UpdateSensorAction>
      as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
  uint32_t sensor_serial_;

 public:
  AccerionUpdateSensorServer(std::string name, uint32_t sensor_serial);
  void ExecuteCallback(const accerion_driver_msgs::UpdateSensorGoalConstPtr &goal);
};
#endif // ACCERIONUPDATESENSORERVERROS_H