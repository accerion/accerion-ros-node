/* Copyright (c) 2017-2021, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <accerion_driver_msgs/RecordingsAction.h>

#include "AccerionSensorAPI/sensor.h"

class AccerionRecordingsServer {
 protected:
  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<accerion_driver_msgs::RecordingsAction>
      as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
  std::shared_ptr<accerion::api::Sensor> sensor_ = nullptr;

 public:
  AccerionRecordingsServer(std::string name, std::shared_ptr<accerion::api::Sensor> sensor);
  void ExecuteCallback(const accerion_driver_msgs::RecordingsGoalConstPtr &goal);
};
