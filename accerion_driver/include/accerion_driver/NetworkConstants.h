// /* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
//  * All rights reserved.
//  *
//  * This Source Code Form is subject to the terms of the Mozilla Public
//  * License, v. 2.0. If a copy of the MPL was not distributed with this
//  * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

// #ifndef NETWORKCONSTANTS_H_
// #define NETWORKCONSTANTS_H_

// #include <string>

// namespace NetworkConstants
// {
//     const std::string               multicastRangeFirstIPAddress    = "224.1.0.0";
//     const std::string               multicastRangeLastIPAddress     = "224.1.255.255";
//     const std::string               defaultMulticastIPAddress       = "224.1.0.10";
//     const std::string               localhostIPAddress              = "127.0.0.1";
//     static constexpr unsigned int   maximumNumOfBytesInUDPMessage   = 65507;
//     static constexpr unsigned int   socketSendBufferSize            = 2000000;
//     static constexpr unsigned int   TCPBufferSize                   = 100000000;
// }

// #endif