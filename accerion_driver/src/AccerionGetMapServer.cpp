#include "AccerionGetMapServer.h"

AccerionGetMapServer::AccerionGetMapServer(std::string name, std::shared_ptr<accerion::api::Sensor> sensor) :
    as_(nh_, name, boost::bind(&AccerionGetMapServer::ExecuteCallback, this, _1), false),
    action_name_(name) {
  sensor_ = sensor;
  as_.start();
}

void AccerionGetMapServer::ExecuteCallback(const accerion_driver_msgs::GetMapGoalConstPtr &goal) {

  accerion_driver_msgs::GetMapFeedback feedback;
  accerion_driver_msgs::GetMapResult result;

  auto progress_lambda = [&feedback, this](int progress) {
    ROS_INFO("Map retrieval progress: %i", progress);
    feedback.progress = progress;
    as_.publishFeedback(feedback);
  };

  std::mutex m;
  std::condition_variable cv;

  auto done_lambda = [&result, &m, &cv](bool res, std::string msg) {
    ROS_INFO("%s", msg.c_str());

    result.done = true;
    result.success = res;
    result.message = msg;
    std::unique_lock<std::mutex> lk(m);
    lk.unlock();
    cv.notify_all();
  };

  sensor_->GetMap(goal->path, std::move(progress_lambda), std::move(done_lambda));

  std::unique_lock<std::mutex> lk(m);
  cv.wait(lk, [&result] { return result.done; });

  result.success ? as_.setSucceeded(result) : as_.setAborted(result);
}