#include "AccerionSubsetServer.h"

AccerionSubsetServer::AccerionSubsetServer(std::string name, std::shared_ptr<accerion::api::Sensor> sensor) :
    as_(nh_, name, boost::bind(&AccerionSubsetServer::ExecuteCallback, this, _1), false),
    action_name_(name) {
  sensor_ = sensor;
  as_.start();
}

void AccerionSubsetServer::ExecuteCallback(const
                                           accerion_driver_msgs::SubsetMapGoalConstPtr &goal) {
  accerion_driver_msgs::SubsetMapFeedback feedback;
  accerion_driver_msgs::SubsetMapResult result;

  auto progress_lambda = [&feedback, this](int progress) {
    ROS_INFO("Map subset progress: %i", progress);
    feedback.progress = progress;
    as_.publishFeedback(feedback);
  };

  std::mutex m;
  std::condition_variable cv;

  auto done_lambda = [&result, &m, &cv](accerion::api::SubsetResult subset_result, std::vector<accerion::api::ClusterId> cl) {
    std::string msg = "";
    bool res = false;

    switch (subset_result) {
      case accerion::api::SubsetResult::kSuccess: {
        res = true;
        if (cl.size() == 0) {
          msg = "Map subset succeeded!";
        } else {
          std::stringstream sstream;
          sstream << "Map subset was done, however the following "
                  << "ID's were not located: ";
          for (int i = 0; i < cl.size(); i++) {
            sstream << cl[i];
            if (i != (cl.size() - 1)) sstream << ", ";
          }
          sstream << ".";
          msg = sstream.str();
        }
      }
        break;
      case accerion::api::SubsetResult::kAreaIdInUse:res = false;
        msg = "Map subset failed, areaID already in use";
        break;
      case accerion::api::SubsetResult::kDiskFull:res = false;
        msg = "Map subset failed, there is no space left "
              "on the sensor";
        break;
      case accerion::api::SubsetResult::kNoInput:res = false;
        msg = "Map subset failed, please provide input";
        break;
    }

    result.done = true;
    result.success = res;
    result.message = msg;
    result.failedIDs = cl;
    std::unique_lock<std::mutex> lk(m);
    lk.unlock();
    cv.notify_all();
  };

  accerion::api::AreaId target_area = static_cast<accerion::api::AreaId>(goal->targetAreaID);
  sensor_->SetCreateMapSubsetCallbacks(std::move(done_lambda), std::move(progress_lambda));
  sensor_->CreateMapSubset(goal->clusterIDs, target_area);

  std::unique_lock<std::mutex> lk(m);
  cv.wait(lk, [&result] { return result.done; });

  result.success ? as_.setSucceeded(result) : as_.setAborted(result);
}