/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionDriverROS.h"

#include <chrono>
#include <cstdint>
#include <functional>
#include <limits>
#include <memory>
#include <sstream>
#include <utility>
#include <vector>

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/utils/memory.h"
#include "AccerionSensorAPI/utils/sensor.h"
#include "AccerionSensorAPI/connection_manager.h"

namespace detail {

bool IdIsWithinLimits(accerion::api::ClusterId cluster_id, accerion::api::SignatureId signature_id) {
  if (cluster_id > std::numeric_limits<std::uint32_t>::max() ||
      signature_id > std::numeric_limits<std::uint32_t>::max()) {
    return false;
  }
  return true;
}

bool PositionIsWithinLimits(double x, double y) {
  if (x > std::numeric_limits<float>::max() ||
      y > std::numeric_limits<float>::max()) {
    return false;
  }
  return true;
}

}  //  namespace detail

AccerionDriverROS::AccerionDriverROS() {
  pnh_ = accerion::api::MakeUnique<ros::NodeHandle>("~");

  node_ns_ = ros::this_node::getName();
  ROS_INFO("node_ns_: %s", node_ns_.c_str());

  LoadParameters();
  InitPublishersAndSubscribers();
  InitServices();

  //initialize other variables
  correction_counter_ = 0;
  InitSignatureMarker();
}

void AccerionDriverROS::LoadParameters() {
  // read ros params
  sensor_serial_ = pnh_->param("serial_number", 0);
  connection_type_ = pnh_->param<std::string>("connection_type", "CONNECTION_TCP");
  ext_ref_timestamp_compensation_ = pnh_->param("ext_ref_timestamp_compensation", true);
  ext_ref_pose_latency_ = pnh_->param("ext_ref_latency", 0.0);
  global_parent_frame_ = pnh_->param<std::string>("global_parent_frame", "map");
  local_parent_frame_ = pnh_->param<std::string>("local_parent_frame", "odom");
  sensor_link_ = pnh_->param<std::string>("sensor_link", "sensor_link");
  ext_ref_pose_topic_ = pnh_->param<std::string>("ext_ref_topic", "ext_ref");
  search_range_topic_ = pnh_->param<std::string>("search_range_topic", "search_range");
  local_ip_ = pnh_->param<std::string>("local_ip",default_local_ip_);
  output_timestamp_compensation_ = pnh_->param("output_timestamp_compensation", 0.0);
}

void AccerionDriverROS::InitPublishersAndSubscribers() {
  // initialize publishers and subscribers
  subs_external_odometry_ = nh_.subscribe(ext_ref_pose_topic_,
                                        1,
                                        &AccerionDriverROS::CallbackExternalReferencePose,
                                        this,
                                        ros::TransportHints().tcpNoDelay());
  sub_search_range_ = nh_.subscribe(search_range_topic_,
                                          1,
                                        &AccerionDriverROS::CallbackCorrectionSearchRange,
                                        this,
                                        ros::TransportHints().tcpNoDelay());

  pub_odom_ = nh_.advertise<nav_msgs::Odometry>("odom", 1);
  pub_corrected_odom_ = nh_.advertise<nav_msgs::Odometry>("corrected_odom", 1);
  pub_drift_details_ = nh_.advertise<accerion_driver_msgs::DriftCorrectionDetails>("correction_details", 1);
  pub_diagnostics_ = pnh_->advertise<accerion_driver_msgs::SensorDiagnostics>("diagnostics", 1);
  pub_line_follower_ = nh_.advertise<geometry_msgs::PoseArray>("line_follower", 1);
  pub_line_follower_visualization_ = nh_.advertise<geometry_msgs::PolygonStamped>("line_follower_visualization", 1);
  pub_created_signatures_ = nh_.advertise<accerion_driver_msgs::CreatedSignature>("created_signature", 1);
  pub_global_corrections_ = nh_.advertise<nav_msgs::Odometry>("corrections", 1);
  pub_map_as_point_cloud2_ = nh_.advertise<sensor_msgs::PointCloud2>("map", 10, true);
  pub_marker_visualization_ = nh_.advertise<visualization_msgs::MarkerArray>("marker_in_sensor_frame", 5, false);
}

void AccerionDriverROS::InitServices() {
  service_server_buffered_recovery_mode_set_buffer_length__ = nh_.advertiseService("set_buffer_length",
                                                                          &AccerionDriverROS::CallbackServiceBufferedRecoveryModeSetBufferLength,
                                                                          this);
  service_server_buffered_recovery_mode_get_buffer_length__ = nh_.advertiseService("get_buffer_length",
                                                                          &AccerionDriverROS::CallbackServiceBufferedRecoveryModeGetBufferLength,
                                                                          this);
  service_server_buffered_recovery_mode_start_ = nh_.advertiseService("start_recovery",
                                                                &AccerionDriverROS::CallbackServiceStartBufferedRecoveryMode,
                                                                this);
  service_server_buffered_recovery_mode_stop_ = nh_.advertiseService("stop_recovery",
                                                                &AccerionDriverROS::CallbackServiceStopBufferedRecoveryMode,
                                                                this);
  service_server_delete_area_ =
      nh_.advertiseService("delete_area", &AccerionDriverROS::CallbackServiceDeleteArea, this);
  service_server_delete_cluster_ =
      nh_.advertiseService("delete_cluster", &AccerionDriverROS::CallbackServiceDeleteCluster, this);
  service_server_mode_ = nh_.advertiseService("mode", &AccerionDriverROS::CallbackServiceMode, this);
  service_server_mode_with_cluster_ =
      nh_.advertiseService("mode_with_cluster", &AccerionDriverROS::CallbackServiceModeWithCluster, this);
  service_server_request_ = nh_.advertiseService("request", &AccerionDriverROS::CallbackServiceRequest, this);
  service_server_set_corrected_odometry_ = nh_.advertiseService("set_corrected_odometry", &AccerionDriverROS::CallbackServiceSetCorrectedOdometry, this);
  service_server_set_internal_mode_ =
      nh_.advertiseService("set_internal_mode", &AccerionDriverROS::CallbackServiceSetInternalMode, this);
  service_server_set_udp_settings_ =
      nh_.advertiseService("set_udp_settings", &AccerionDriverROS::CallbackServiceSetUDPSettings, this);

  service_server_save_images_ = nh_.advertiseService("set_save_images", &AccerionDriverROS::CallbackServiceSetSaveImages, this);

  service_server_recordings_ = nh_.advertiseService("get_recordings_list", &AccerionDriverROS::CallbackServiceRecordings, this);

  service_server_set_ip_ = nh_.advertiseService("set_ip", &AccerionDriverROS::CallbackServiceSetIPAddress, this);
  service_server_set_date_time_ =
      nh_.advertiseService("set_datetime", &AccerionDriverROS::CallbackServiceSetDateTime, this);
  service_server_get_areas_ = nh_.advertiseService("get_areas", &AccerionDriverROS::CallbackServiceGetAreas, this);
  service_server_set_area_ = nh_.advertiseService("set_area", &AccerionDriverROS::CallbackServiceSetArea, this);
  service_server_get_ram_rom_stats_ =
      nh_.advertiseService("get_ram_rom_stats", &AccerionDriverROS::CallbackServiceGetRamRomStats, this);
  service_server_log_event_ = nh_.advertiseService("log_event", &AccerionDriverROS::CallbackServiceLogEvent, this);
}

void AccerionDriverROS::InitSignatureMarker() {
  map_point_cloud2_.header.frame_id = global_parent_frame_;
  map_point_cloud2_.is_bigendian = false;
  map_point_cloud2_.is_dense = true;

  map_point_cloud2_.width = 0;
  map_point_cloud2_.height = 1;
  map_point_cloud2_.fields.resize(6);

  map_point_cloud2_.point_step = 36;
  map_point_cloud2_.row_step = map_point_cloud2_.point_step * map_point_cloud2_.width;

  map_point_cloud2_.data.resize(map_point_cloud2_.width * map_point_cloud2_.point_step);

  point_cloud2_modifier_ = accerion::api::MakeUnique<sensor_msgs::PointCloud2Modifier>(map_point_cloud2_);
  point_cloud2_modifier_->setPointCloud2Fields(6, "x", 1, sensor_msgs::PointField::FLOAT32, // FLOAT64 does not work with RVIZ position transformer
                                    "y", 1, sensor_msgs::PointField::FLOAT32,
                                    "z", 1, sensor_msgs::PointField::FLOAT32,
                                    "th", 1, sensor_msgs::PointField::FLOAT64,
                                    "signature_id", 1, sensor_msgs::PointField::UINT32,  // UINT32 is the largest int available in PointField
                                    "cluster_id", 1, sensor_msgs::PointField::UINT32);   // UINT32 is the largest int available in PointField
}

int AccerionDriverROS::run() {
  std::string version = +ACCERIONROSNODE_VERSION;
  ROS_INFO("%s: Starting Accerion Ros Node version: %s", node_ns_.c_str(), version.c_str());
  sleep(5);

  ConnectToSensor();
  SubscribeToSensorMsgs();
  InitActions();

  return 0;
}

void AccerionDriverROS::ConnectToSensor() {
  accerion::api::Address local_ip {accerion::api::Address::Parse(local_ip_)};

  auto selected_connection_type = GetSelectedConnectionType();  

  if (selected_connection_type == accerion::api::ConnectionType::kConnectionUdpUnicast && 
      local_ip == accerion::api::Address::Parse(default_local_ip_)) {
    ROS_ERROR("%s: UDP Unicast connection requested. Please provide valid local ip.", node_ns_.c_str());
    ros::requestShutdown();
  }

  if (sensor_serial_ == 0) {
    ROS_WARN("%s: No serial given, will connect to the first detected serial number..", node_ns_.c_str());
    accerion_sensor_ = accerion::api::GetAnySensor(selected_connection_type, local_ip);
    sensor_serial_ = accerion_sensor_->GetSerialNumber().value;
  } else {
    auto& future_sensor = accerion::api::ConnectionManager::GetSensor(accerion::api::SerialNumber{sensor_serial_},
                            selected_connection_type, local_ip);
    accerion_sensor_ = future_sensor.WaitFor(std::chrono::seconds(5));
  }
}

accerion::api::ConnectionType AccerionDriverROS::GetSelectedConnectionType() const {
  if (connection_type_ == "CONNECTION_UDP_BROADCAST") {
      return accerion::api::ConnectionType::kConnectionUdpBroadcast;
  } else if (connection_type_ == "CONNECTION_UDP_UNICAST") {
      return accerion::api::ConnectionType::kConnectionUdpUnicast;
  } else if (connection_type_ == "CONNECTION_TCP_DISABLE_UDP") {
      return accerion::api::ConnectionType::kConnectionTcpDisableUdp;
  } else {
      return accerion::api::ConnectionType::kConnectionTcp;
  }
}

void AccerionDriverROS::SubscribeToSensorMsgs() {
  ROS_INFO("%s: Subscribing to API topics..", node_ns_.c_str());
  // connect everything
  accerion_sensor_->SubscribeToCorrectedOdometry().Callback(
      std::bind(&AccerionDriverROS::CorrectedOdometryAck, this, std::placeholders::_1));
  accerion_sensor_->SubscribeToOdometry().Callback(
      std::bind(&AccerionDriverROS::OdometryAck, this, std::placeholders::_1));
  accerion_sensor_->SubscribeToDiagnostics().Callback(
      std::bind(&AccerionDriverROS::DiagnosticsAck, this, std::placeholders::_1));
  accerion_sensor_->SubscribeToCompensatedDriftCorrection().Callback(
      std::bind(&AccerionDriverROS::DriftCorrectionAck, this, std::placeholders::_1));
  accerion_sensor_->SubscribeToLineFollowerData().Callback(
      std::bind(&AccerionDriverROS::LineFollowerAck, this, std::placeholders::_1));
  accerion_sensor_->SubscribeToMarkers().Callback(
      std::bind(&AccerionDriverROS::MarkerAck, this, std::placeholders::_1));
  accerion_sensor_->SubscribeToConsoleOutput().Callback([](const accerion::api::PlainString& output){ ROS_INFO("ACK_CONSOLE_OUTPUT_INFO: %s", output.value.c_str()); });
  accerion_sensor_->SubscribeToCreatedSignature().Callback(
      std::bind(&AccerionDriverROS::CreatedSignatureAck, this, std::placeholders::_1));
  ROS_INFO("%s: Subscribing done!", node_ns_.c_str());
}

void AccerionDriverROS::InitActions() {
  accerion_search_loop_closures_server_ = accerion::api::MakeUnique<AccerionSearchLoopClosuresServer>("search_loop_closures", accerion_sensor_);
  accerion_getMap_server_ = accerion::api::MakeUnique<AccerionGetMapServer>("get_map", accerion_sensor_);
  accerion_sendMap_server_ = accerion::api::MakeUnique<AccerionSendMapServer>("send_map", accerion_sensor_);
  accerion_subset_server_ = accerion::api::MakeUnique<AccerionSubsetServer>("subset_map", accerion_sensor_);
  accerion_get_logs_server_ = accerion::api::MakeUnique<AccerionGetLogsServer>("get_logs", sensor_serial_);
  accerion_recordings_server_ = accerion::api::MakeUnique<AccerionRecordingsServer>("recordings", accerion_sensor_);
  accerion_update_sensor_server_ = accerion::api::MakeUnique<AccerionUpdateSensorServer>("update_sensor", sensor_serial_);
}

ros::Time AccerionDriverROS::LatencyCompensatedTimestamp() const {
  return ros::Time::now() - ros::Duration(0, output_timestamp_compensation_ * 1e6);
}

void AccerionDriverROS::CorrectedOdometryAck(accerion::api::CorrectedOdometry corrected_odometry)
{
  const auto ros_time_stamp = LatencyCompensatedTimestamp();

  nav_msgs::Odometry global_pose_msg;
  global_pose_msg.header.stamp = ros_time_stamp;
  global_pose_msg.header.frame_id = global_parent_frame_;
  global_pose_msg.child_frame_id = sensor_link_;
  global_pose_msg.pose.pose.position.x = corrected_odometry.pose.x;
  global_pose_msg.pose.pose.position.y = corrected_odometry.pose.y;

  global_pose_msg.pose.pose.orientation =
      tf::createQuaternionMsgFromYaw(corrected_odometry.pose.th * accerion::api::kDegToRad);

  global_pose_msg.pose.covariance[0] = std::pow(corrected_odometry.standard_deviation.x, 2);
  global_pose_msg.pose.covariance[7] = std::pow(corrected_odometry.standard_deviation.y, 2);
  global_pose_msg.pose.covariance[35] =
      std::pow(corrected_odometry.standard_deviation.th * accerion::api::kDegToRad, 2);

  pub_corrected_odom_.publish(global_pose_msg);
}

void AccerionDriverROS::OdometryAck(accerion::api::Odometry odom)
{
  ros::Time ros_time_stamp = LatencyCompensatedTimestamp();

  nav_msgs::Odometry local_odom_msg;
  local_odom_msg.header.stamp = ros_time_stamp;
  local_odom_msg.header.frame_id = local_parent_frame_;
  local_odom_msg.child_frame_id = sensor_link_;

  local_odom_msg.pose.pose.position.x = odom.pose.x;
  local_odom_msg.pose.pose.position.y = odom.pose.y;
  local_odom_msg.pose.pose.orientation = tf::createQuaternionMsgFromYaw(odom.pose.th * accerion::api::kDegToRad);

  local_odom_msg.twist.twist.linear.x = odom.velocity.x;
  local_odom_msg.twist.twist.linear.y = odom.velocity.y;
  local_odom_msg.twist.twist.angular.z = odom.velocity.th * accerion::api::kDegToRad;

  local_odom_msg.twist.covariance[0] = std::pow(odom.standard_deviation_velocity.x, 2);
  local_odom_msg.twist.covariance[7] = std::pow(odom.standard_deviation_velocity.y, 2);
  local_odom_msg.twist.covariance[35] = std::pow(odom.standard_deviation_velocity.th * accerion::api::kDegToRad, 2);

  pub_odom_.publish(local_odom_msg);
}

void AccerionDriverROS::DiagnosticsAck(accerion::api::Diagnostics diag) {
  ros::Time ros_time_stamp = ros::Time::now();
  accerion_driver_msgs::SensorDiagnostics diagnostics_msg;

  diagnostics_msg.header.stamp = ros_time_stamp;
  diagnostics_msg.serial_number = sensor_serial_;
  diagnostics_msg.raw_timestamp = diag.timestamp.time_since_epoch().count();

  diagnostics_msg.modes_code = diag.modes;
  std::bitset<16> modes_bitset(diagnostics_msg.modes_code);

  diagnostics_msg.warnings_code = diag.warning_codes;
  std::bitset<16> warnings_bitset(diagnostics_msg.warnings_code);

  diagnostics_msg.errors_code = diag.error_codes;
  std::bitset<32> errors_bitset(diagnostics_msg.errors_code);

  for (unsigned int ii = 0; ii < 32; ii++) {
    if (ii < 16) {
      if (modes_bitset.test(ii))
        diagnostics_msg.active_modes += accerion::api::kModeDescriptionMap.find(ii)->second + ",";

      if (warnings_bitset.test(ii))
        diagnostics_msg.active_warnings += accerion::api::kWarningMap.find(ii)->second + ",";
    }

    if (errors_bitset.test(ii))
      diagnostics_msg.active_errors += accerion::api::kErrorMap.find(ii)->second + ",";
  }

  pub_diagnostics_.publish(diagnostics_msg);
}

void AccerionDriverROS::DriftCorrectionAck(accerion::api::DriftCorrection drift_correction)
{
  nav_msgs::Odometry corrected_pose_message;
  corrected_pose_message.header.stamp = LatencyCompensatedTimestamp();
  corrected_pose_message.header.seq = correction_counter_;

  corrected_pose_message.header.frame_id = global_parent_frame_;
  corrected_pose_message.child_frame_id = sensor_link_;

  corrected_pose_message.pose.pose.position.x = drift_correction.pose.x;
  corrected_pose_message.pose.pose.position.y = drift_correction.pose.y;
  corrected_pose_message.pose.pose.orientation =
      tf::createQuaternionMsgFromYaw(drift_correction.pose.th * accerion::api::kDegToRad);

  corrected_pose_message.pose.covariance[0] = std::pow(drift_correction.standard_deviation.x, 2);
  corrected_pose_message.pose.covariance[7] = std::pow(drift_correction.standard_deviation.y, 2);
  corrected_pose_message.pose.covariance[35] =
      std::pow(drift_correction.standard_deviation.th * accerion::api::kDegToRad, 2);

  ++correction_counter_;

  pub_global_corrections_.publish(corrected_pose_message);

  accerion_driver_msgs::DriftCorrectionDetails correction_details_message;
  correction_details_message.pose = corrected_pose_message.pose.pose;

  correction_details_message.error.position.x = drift_correction.delta.x;
  correction_details_message.error.position.y = drift_correction.delta.y;
  correction_details_message.error.orientation =
      tf::createQuaternionMsgFromYaw(drift_correction.delta.th * accerion::api::kDegToRad);

  correction_details_message.cumulative_traveled_distance = drift_correction.cumulative_travelled_distance;
  correction_details_message.cumulative_traveled_rotation =
      drift_correction.cumulative_travelled_heading * accerion::api::kDegToRad;
  correction_details_message.error_percentage = drift_correction.error_percentage;

  correction_details_message.cluster_id = drift_correction.cluster_id;
  correction_details_message.signature_id = drift_correction.signature_id;
  correction_details_message.quality_estimate = drift_correction.quality_estimate;

  pub_drift_details_.publish(correction_details_message);
}

void AccerionDriverROS::LineFollowerAck(accerion::api::LineFollowerData lfd) {
  ros::Time ros_time_stamp = LatencyCompensatedTimestamp();

  geometry_msgs::Point32 sensor_point, closest_point_on_line;

  sensor_point.x = lfd.pose.x;
  sensor_point.y = lfd.pose.y;
  sensor_point.z = 0.0;

  closest_point_on_line.x = lfd.closest_point.x;
  closest_point_on_line.y = lfd.closest_point.y;
  closest_point_on_line.z = 0.0;

  geometry_msgs::PolygonStamped line_follower_arrow_polygon;

  line_follower_arrow_polygon.header.frame_id = global_parent_frame_;
  line_follower_arrow_polygon.header.stamp = ros_time_stamp;
  line_follower_arrow_polygon.polygon.points = {sensor_point, closest_point_on_line};
  pub_line_follower_visualization_.publish(line_follower_arrow_polygon);

  geometry_msgs::PoseArray lf_pose_array;
  lf_pose_array.header.frame_id = global_parent_frame_;
  lf_pose_array.header.stamp = ros_time_stamp;
  geometry_msgs::Pose sensor_pose, closest_point;
  sensor_pose.position.x = lfd.pose.x;
  sensor_pose.position.y = lfd.pose.y;
  sensor_pose.orientation =
      tf::createQuaternionMsgFromYaw(lfd.pose.th * accerion::api::kDegToRad);

  closest_point.position.x = lfd.closest_point.x;
  closest_point.position.y = lfd.closest_point.y;
  closest_point.orientation = tf::createQuaternionMsgFromYaw(lfd.closest_point.th * accerion::api::kDegToRad);

  lf_pose_array.poses = {sensor_pose, closest_point};
  pub_line_follower_.publish(lf_pose_array);
}

void AccerionDriverROS::SignatureMarkerAck(accerion::api::SignatureVector vec) {
  map_point_cloud2_.data.clear();
  map_point_cloud2_.width = 0;
  VisualizeSignatures(vec);
}

void AccerionDriverROS::MarkerAck(accerion::api::MarkerDetection m) const {
  visualization_msgs::MarkerArray marker_array;
  visualization_msgs::Marker marker, marker_text, marker_arrow;

  if (m.type != accerion::api::MarkerType::kAruco) {
    return;
  }

  marker.header.frame_id = sensor_link_;
  marker.header.stamp = LatencyCompensatedTimestamp();
  marker.id = std::stoi(m.id);
  std::stringstream ss;
  ss << marker.id;
  marker.ns = ss.str() + "/geometry"; // ss.str();
  marker.type = visualization_msgs::Marker::CUBE;
  marker.action = visualization_msgs::Marker::ADD;
  const float marker_yaw = m.pose.th * accerion::api::kDegToRad;
  marker.pose.position.x = m.pose.x;
  marker.pose.position.y = m.pose.y;
  marker.pose.position.z = 0;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = sin(marker_yaw / 2.0);
  marker.pose.orientation.w = cos(marker_yaw / 2.0);
  marker.frame_locked = false;

  marker_text = marker;
  marker_arrow = marker;

  marker.scale.x = 0.02;
  marker.scale.y = 0.02;
  marker.scale.z = 0.001;
  marker.color.a = 0.5;
  marker.color.r = 255;
  marker.color.g = 255;
  marker.color.b = 255;

  marker_array.markers.push_back(marker);

  marker_text.ns = ss.str() + "/text";
  marker_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  marker_text.text = ss.str();
  marker_text.scale.z = 0.004;
  marker_text.color.a = 1.0;
  marker_text.color.r = 0;
  marker_text.color.g = 1.0;
  marker_text.color.b = 1.0;

  marker_array.markers.push_back(marker_text);

  marker_arrow.ns = ss.str() + "/arrow";
  marker_arrow.type = visualization_msgs::Marker::ARROW;
  marker_arrow.color.a = 0.5;
  marker_arrow.scale.x = 0.01;
  marker_arrow.scale.y = 0.0015;
  marker_arrow.scale.z = marker_arrow.scale.y;
  marker_arrow.color.r = 255;

  marker_array.markers.push_back(marker_arrow);

  pub_marker_visualization_.publish(marker_array);
}

void AccerionDriverROS::BufferedRecoveryModeProgressIndicator(accerion::api::BufferProgress buffer_progress) const {
  switch (buffer_progress.message_type) {
    case accerion::api::BufferedRecoveryMessageType::kProgress:
      ROS_INFO("Buffered Recovery Mode Progress: %i", buffer_progress.progress);
      break;
    case accerion::api::BufferedRecoveryMessageType::kFinished: {
        std::string result = buffer_progress.result ? "Success" : "Failure";
        ROS_INFO("Buffered Recovery Mode Complete: %s", result.c_str());
        break;
      }
    case accerion::api::BufferedRecoveryMessageType::kCanceled:
      ROS_INFO("Buffered Recovery Mode Cancelled");
      break;
    case accerion::api::BufferedRecoveryMessageType::kLowOverlap:
      ROS_INFO("Buffered Recovery Mode Low Overlap: %i", buffer_progress.percentage_of_low_overlap);
      break;
    case accerion::api::BufferedRecoveryMessageType::kNoMap:
      ROS_ERROR("Buffered Recovery Mode: Current map is empty!");
      break;
    case accerion::api::BufferedRecoveryMessageType::kNoSignatures:
      ROS_ERROR("Buffered Recovery Mode: No signatures within search radius!");
      break;
    case accerion::api::BufferedRecoveryMessageType::kEmpty:
      ROS_ERROR("Buffered Recovery Mode: Buffer is empty!");
      break;
  }
}

void AccerionDriverROS::CallbackExternalReferencePose(const geometry_msgs::PoseStamped::ConstPtr& ext_ref_msg) {
  std::uint64_t time_offset_usec = 0;
  if (ext_ref_timestamp_compensation_) {
    const auto time_now = ros::Time::now();

    ros::Duration time_offset;
    if (ext_ref_msg->header.stamp > time_now) {
      ROS_WARN("Timestamp of the external reference is in the future! Ignoring it and setting time offset to zero");
      time_offset = ros::Duration(0.0);
    } else {
      time_offset = time_now - ext_ref_msg->header.stamp;
      if (time_offset.toSec() > 1.0) {
        ROS_WARN("Time offset of the external reference message exceeds 1 second!");
      }
    }
    time_offset_usec = static_cast<uint64_t>(time_offset.toNSec() / 1e3);
  }
  // Additional latency on top of ROS timestamp compensation
  time_offset_usec = time_offset_usec + ext_ref_pose_latency_ * 1e3;

  accerion::api::ExternalReferencePose ext_ref_pose;
  ext_ref_pose.time_offset = std::chrono::microseconds{time_offset_usec};
  ext_ref_pose.pose.x = ext_ref_msg->pose.position.x;
  ext_ref_pose.pose.y = ext_ref_msg->pose.position.y;
  ext_ref_pose.pose.th = tf::getYaw(ext_ref_msg->pose.orientation) * accerion::api::kRadToDeg;

  const auto duration =
      std::chrono::seconds(ext_ref_msg->header.stamp.sec) + std::chrono::nanoseconds(ext_ref_msg->header.stamp.nsec);
  ext_ref_pose.timestamp = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>(duration);

  accerion_sensor_->SetExternalReference(std::move(ext_ref_pose));
}

void AccerionDriverROS::CallbackCorrectionSearchRange(const accerion_driver_msgs::SearchRange& sr) {
  accerion::api::SearchRange sr_out{sr.radius, sr.angle * accerion::api::kRadToDeg};
  accerion_sensor_->SetSearchRange(std::move(sr_out));
}

bool AccerionDriverROS::CallbackServiceDeleteCluster(accerion_driver_msgs::Cluster::Request &req,
                                                     accerion_driver_msgs::Cluster::Response &res) {
  auto result = accerion_sensor_->DeleteCluster(req.cluster_id).WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occured when trying to delete a cluster. The acknowledgement was not received, but the cluster might have been removed";
    return true;
  }
  if (result.value().accepted) {
      res.success = true;
      res.message = "Cluster succesfully deleted!";
  } else {
      res.success = false;
      res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceDeleteArea(accerion_driver_msgs::DeleteArea::Request &req,
                                                  accerion_driver_msgs::DeleteArea::Response &res) {
  accerion::api::Optional<accerion::api::AreaId> area_id;
  if (!req.delete_all) {
    area_id = accerion::api::Optional<accerion::api::AreaId>(req.area_to_be_deleted);
  }
  auto result = accerion_sensor_->DeleteArea(area_id).WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occured when trying to delete area(s). The acknowledgement was not received, but the area(s) might have been removed";
    return true;
  }
  if (result.value().accepted) {
      res.success = true;
      res.message = "Area(s) succesfully deleted!";
  } else {
      res.success = false;
      res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceBufferedRecoveryModeSetBufferLength(accerion_driver_msgs::SetBufferedRecoveryBufferLength::Request &req,
                                                                           accerion_driver_msgs::SetBufferedRecoveryBufferLength::Response &res) {
  auto result = accerion_sensor_->SetBufferLength(req.bufferLength).WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occured while trying to set buffered recovery buffer length. Acknowledgement not received, but might have been set";
    return true;
  }
  if (result.value().accepted) {
      res.success = true;
      res.message = "Buffered recovery mode buffer length is set!";
  } else {
      res.success = false;
      res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceBufferedRecoveryModeGetBufferLength(accerion_driver_msgs::EmptyCommand::Request &req,
                                                                           accerion_driver_msgs::EmptyCommand::Response &res) {
  auto result = accerion_sensor_->GetBufferLength().WaitFor();
  if (!result) {
    res.success = false;
    res.message =
        "A timeout occured while trying to get buffered recovery buffer length. Acknowledgement not received.";
  } else {
    res.success = true;
    res.message = "Buffered recovery mode buffer length is " + std::to_string(result.value().buffer_length);
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceStartBufferedRecoveryMode(accerion_driver_msgs::StartBufferedRecoveryMode::Request &req,
                                                                 accerion_driver_msgs::StartBufferedRecoveryMode::Response &res) {
  auto& future = accerion_sensor_->StartBufferedRecovery(req.xPos, req.yPos, req.radius, req.fullMapSearch);
  future.Callback(std::bind(&AccerionDriverROS::BufferedRecoveryModeProgressIndicator, this, std::placeholders::_1));
  auto result = future.WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occured while trying to starting buffered recovery mode. Acknowledgement not received, but might have started";
    return true;
  }
  auto message = result.value();
  if (message.message_type == accerion::api::BufferedRecoveryMessageType::kProgress) {
      res.success = true;
      res.message = "Buffered recovery mode is started succesfully!";
  } else {
      res.success = false;
      res.message = "Buffered recovery mode is not started!";
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceStopBufferedRecoveryMode(accerion_driver_msgs::EmptyCommand::Request &req,
                                                                accerion_driver_msgs::EmptyCommand::Response &res) {
  auto result = accerion_sensor_->CancelBufferedRecovery().WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occured while trying to stop buffered recovery mode. Acknowledgement not received, but might have stopped";
    return true;
  }
  if (result.value().message_type == accerion::api::BufferedRecoveryMessageType::kCanceled &&
        !result.value().result) {
      res.success = true;
      res.message = "Buffered recovery mode is stopped succesfully!";
  } else {
      res.success = false;
      res.message = "Buffered recovery mode is not stopped!";
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceMode(accerion_driver_msgs::ModeCommand::Request &req,
                                            accerion_driver_msgs::ModeCommand::Response &res) {
  if (req.command.compare("start") != 0 && req.command.compare("stop") != 0) {
    res.success = false;
    res.message = "usage: rosservice call " + service_server_mode_.getService()
        + " localization/idle/recording/reboot/aruco start/stop";
    return true;
  }
  bool start = true;
  if (req.command.compare("stop") == 0)
    start = false;

  accerion::api::Optional<accerion::api::Acknowledgement> result;
  if (req.mode.compare("localization") == 0) {
    result = accerion_sensor_->SetLocalizationMode(start).WaitFor();
  } else if (req.mode.compare("idle") == 0) {
    result = accerion_sensor_->SetIdleMode(start).WaitFor();
  } else if (req.mode.compare("recording") == 0) {
    result = accerion_sensor_->SetRecordingMode(start).WaitFor();
  } else if (req.mode.compare("reboot") == 0) {
    result = accerion_sensor_->Reboot().WaitFor();
  } else if (req.mode.compare("aruco") == 0) {
    result = accerion_sensor_->SetMarkerDetectionMode(start, accerion::api::MarkerType::kAruco).WaitFor();
  } else if (req.mode.compare("continuous_signature_updating") == 0) {
    result = accerion_sensor_->SetContinuousSignatureUpdatingMode(start).WaitFor();
  } else {
    res.success = false;
    res.message = "usage: rosservice call " + service_server_mode_.getService()
        + " localization/idle/recording/reboot/aruco/continuous_signature_updating start/stop";
    return true;
  }
  if (!result) {
      res.success = false;
      res.message = "A timeout occured while trying to set mode. Acknowledgement not received, but might have been set nonetheless";
      return true;
  }
  if (result.value().accepted) {
    res.success = true;
    if (start) {
      res.message = "Mode succesfully turned on";
    } else {
      res.message = "Mode succesfully turned off";
    }
  } else {
      res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceModeWithCluster(accerion_driver_msgs::ModeClusterCommand::Request &req,
                                                       accerion_driver_msgs::ModeClusterCommand::Response &res) {
  if (req.command.compare("start") != 0 && req.command.compare("stop") != 0) {
    res.success = false;
    res.message = "usage: rosservice call " + service_server_mode_with_cluster_.getService()
        + " mapping/active_mapping/line_following 42 start/stop";
    return true;
  }

  bool start = true;
  if (req.command.compare("stop") == 0)
    start = false;

  accerion::api::Optional<accerion::api::Acknowledgement> result;
  if (req.mode.compare("mapping") == 0) {
    result = accerion_sensor_->SetMappingMode(start, req.cluster_id).WaitFor();
  } else if (req.mode.compare("line_following") == 0) {
    result = accerion_sensor_->SetLineFollowingMode(start, req.cluster_id).WaitFor();
  } else if (req.mode.compare("active_mapping") == 0) {
    result = accerion_sensor_->SetExpertMode(start).WaitFor();
    if (result && result.value().accepted) {
      result = accerion_sensor_->SetMappingMode(start, req.cluster_id).WaitFor();
    }
  } else {
    res.success = false;
    res.message = "usage: rosservice call " + service_server_mode_with_cluster_.getService()
        + " mapping/active_mapping/line_following 42 start/stop";
    return true;
  }
  if (!result) {
      res.success = false;
      res.message = "A timeout occured while trying to set mode. Acknowledgement not received, but might have been set nonetheless";
      return true;
  }
  if (result.value().accepted) {
    res.success = true;
    if (start) {
      res.message = "Mode succesfully turned on";
    } else {
      res.message = "Mode succesfully turned off";
    }
  } else {
    res.success = false;
    res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceRequest(accerion_driver_msgs::RequestCommand::Request &req,
                                               accerion_driver_msgs::RequestCommand::Response &res) {
  if (req.request_command.compare("map") == 0) {
    accerion_sensor_->GetSignatureInformation().Callback(std::bind(&AccerionDriverROS::SignatureMarkerAck, this, std::placeholders::_1));
  } else if (req.request_command.compare("version") == 0) {
    accerion_sensor_->GetSoftwareVersion().Callback([](const accerion::api::SoftwareVersion sv){ 
                                                      ROS_INFO("ACK_SOFTWARE_VERSION: %s", sv.String().c_str());
                                                    });
  } else if (req.request_command.compare("version_hash") == 0) {
    accerion_sensor_->GetSoftwareDetails().Callback([](const accerion::api::SoftwareDetails& sd) {
                                                      ROS_INFO("ACK_SOFTWARE_HASH %s %s", sd.software_hash.c_str(),
                                                      sd.date.c_str());
                                                    });
  } else if (req.request_command.compare("ip_address") == 0) {
    accerion_sensor_->GetIpAddress().Callback(std::bind(&AccerionDriverROS::IpAddressAck, this, std::placeholders::_1));
  } else if (req.request_command.compare("serial_number") == 0) {
    const auto serial_number =
        std::to_string(accerion_sensor_->GetSerialNumber().value);
    res.message = serial_number;
    ROS_INFO("Current Serial Number: %s", serial_number.c_str());
    return true;
  } else {
    res.success = false;
    res.message = "usage: rosservice call " + service_server_request_.getService()
        + " map/version/version_hash/ip_address/serial_number";
    return true;
  }
  res.success = true;
  return true;
}

bool AccerionDriverROS::CallbackServiceSetCorrectedOdometry(accerion_driver_msgs::Pose::Request &req,
                                               accerion_driver_msgs::Pose::Response &res) {
  accerion::api::Pose pose;
  pose.x = req.x;
  pose.y = req.y;
  pose.th = req.theta * accerion::api::kRadToDeg;

  ROS_INFO("New request to set corrected odometry: %f , %f , %f[rad]", req.x, req.y, req.theta);
  auto result = accerion_sensor_->SetCorrectedOdometry(pose).WaitFor();

  if (!result) {
      res.success = false;
      res.message =
          "A timeout occured while trying to set the pose. Acknowledgement not received, but might have been set";
      return true;
  }
  if (result.value().accepted) {
    res.success = true;
    res.message =
        "Corrected odometry pose set to " + std::to_string(req.x) + "[m]," + std::to_string(req.y) + "[m],"
            + std::to_string(req.theta) + "[rad]";
  } else {
    res.success = false;
    res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceSetInternalMode(std_srvs::SetBool::Request &req,
                                                           std_srvs::SetBool::Response &res) {
  auto result = accerion_sensor_->SetInternalMode(req.data).WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occured while trying to set internal mode. Acknowledgement not received, but might have started";
    return true;
  }
  if (result.value().accepted) {
      res.success = true;
      res.message = "Internal mode is changed succesfully!";
  } else {
      res.success = false;
      res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceSetSaveImages(std_srvs::SetBool::Request &req,
                                                           std_srvs::SetBool::Response &res) {
  auto result = accerion_sensor_->SetSaveImages(req.data).WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occured while trying to set save images. Acknowledgement not received, but might have been set";
    return true;
  }
  if (result.value().accepted) {
      res.success = true;
      res.message = "Save images has been set succesfully!";
  } else {
      res.success = false;
      res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceSetUDPSettings(accerion_driver_msgs::UDPSettings::Request &req,
                                                      accerion_driver_msgs::UDPSettings::Response &res) {
  if ((req.message_type > 0 || req.message_type < 5) && (req.udp_mode == 1 || req.udp_mode == 2)) {
    accerion::api::UdpSettings udp_settings;
    udp_settings.address.first = req.unicast_ip_address_first;
    udp_settings.address.second = req.unicast_ip_address_second;
    udp_settings.address.third = req.unicast_ip_address_third;
    udp_settings.address.fourth = req.unicast_ip_address_fourth;
    udp_settings.message_type = static_cast<accerion::api::EnabledMessageType>(req.message_type);
    udp_settings.strategy = static_cast<accerion::api::UdpStrategy>(req.udp_mode);
    auto result = accerion_sensor_->SetUdpSettings(udp_settings).WaitFor();
    if (!result) {
      res.success = false;
      res.message = "Timeout occurred while setting UDP settings, no acknowledgement received, but settings might have been set.";
      return true;
    }
    res.success = result.value().accepted;
    if (result.value().accepted) {
        res.message = "Setting UDP settings to: unicastIp: " + std::to_string(req.unicast_ip_address_first) + "."
          + std::to_string(req.unicast_ip_address_second) + "." + std::to_string(req.unicast_ip_address_third) + "."
          + std::to_string(req.unicast_ip_address_fourth) + ", message type: " + std::to_string(req.message_type)
          + ", udp mode: " + std::to_string(req.udp_mode) + " . Note: no acknowledgement check is done. TODO";
    } else {
        res.message = result.value().message;
    }
    return true;
  } else {
    res.success = false;
    res.message = "message type can be [1,2,3,4] and udp mode can be [1,2] only.";
    return true;
  }
}

bool AccerionDriverROS::CallbackServiceRecordings(accerion_driver_msgs::RecordingsList::Request &req,
                                                  accerion_driver_msgs::RecordingsList::Response &res) {
  auto result = accerion_sensor_->GetRecordingList().WaitFor();
  if (result) {
    res.recordings = result.value().value;
    res.success = true;
    res.message = "";
  } else {
      res.success = false;
      res.message = "Failure";
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceSetIPAddress(accerion_driver_msgs::SetIP::Request &req,
                                                    accerion_driver_msgs::SetIP::Response &res) {
  accerion::api::IpAddress ip_struct;
  try {
    ip_struct.address = accerion::api::Address::Parse(req.ip);
    ip_struct.netmask = accerion::api::Address::Parse(req.netmask);
    ip_struct.gateway = accerion::api::Address::Parse(req.gateway);
  } catch(...) {
    res.success = false;
    res.message =
        "Couldn't parse the addresses, make sure to provide them in following format 192.168.0.23 255.255.255.0 192.168.0.1";
    return true;
  }

  struct sockaddr_in ip_addr, netmask_addr, gateway_addr;
#ifdef _WIN32
  int ip_result      = InetPton(AF_INET, fullIP.c_str(), &(ip_addr.sin_addr));
  int netmask_result = InetPton(AF_INET, fullNetmask.c_str(), &(netmask_addr.sin_addr));
  int gateway_result = InetPton(AF_INET, fullGateway.c_str(), &(gateway_addr.sin_addr));
#else
  int ip_result = inet_pton(AF_INET, req.ip.c_str(), &(ip_addr.sin_addr));
  int netmask_result = inet_pton(AF_INET, req.netmask.c_str(), &(netmask_addr.sin_addr));
  int gateway_result = inet_pton(AF_INET, req.gateway.c_str(), &(gateway_addr.sin_addr));
#endif
  if (ip_result != 1 || netmask_result != 1 || gateway_result != 1) {
    res.success = false;
    res.message =
        "One of the addresses was invalid, make sure to provide them in following format 192.168.0.23 255.255.255.0 192.168.0.1";
    return true;
  }

  auto result = accerion_sensor_->SetIpAddress(ip_struct).WaitFor();
  res.success = false;
  if (result && result.value().accepted) {
    res.success = true;
    res.message = "The IP address has been changed, the sensor needs to be restarted for changes to take effect. You might also need to restart the ROS node";
    ROS_INFO("static ip address  : %i.%i.%i.%i",
             ip_struct.address.first,
             ip_struct.address.second,
             ip_struct.address.third,
             ip_struct.address.fourth);
    ROS_INFO("static netmask     : %i.%i.%i.%i",
             ip_struct.netmask.first,
             ip_struct.netmask.second,
             ip_struct.netmask.third,
             ip_struct.netmask.fourth);
    ROS_INFO("default gateway    : %i.%i.%i.%i",
             ip_struct.gateway.first,
             ip_struct.gateway.second,
             ip_struct.gateway.third,
             ip_struct.gateway.fourth);
  }
  return true;
}

void AccerionDriverROS::IpAddressAck(accerion::api::IpAddress ip) const {
    ROS_INFO("static ip address  : %i.%i.%i.%i",
             ip.address.first,
             ip.address.second,
             ip.address.third,
             ip.address.fourth);
    ROS_INFO("static netmask     : %i.%i.%i.%i",
             ip.netmask.first,
             ip.netmask.second,
             ip.netmask.third,
             ip.netmask.fourth);
    ROS_INFO("default gateway    : %i.%i.%i.%i",
             ip.gateway.first,
             ip.gateway.second,
             ip.gateway.third,
             ip.gateway.fourth);
}

void AccerionDriverROS::VisualizeSignatures(const accerion::api::SignatureVector& vec) {
  // Resize point cloud to new size
  map_point_cloud2_.width += vec.value.size();
  map_point_cloud2_.header.stamp = ros::Time::now();
  map_point_cloud2_.height = 1;
  map_point_cloud2_.data.resize(map_point_cloud2_.width * map_point_cloud2_.point_step);
  point_cloud2_modifier_->resize(map_point_cloud2_.width);

  // FLOAT64/double does not work with RVIZ position transformer
  sensor_msgs::PointCloud2Iterator<float> iter_x(map_point_cloud2_, "x");
  sensor_msgs::PointCloud2Iterator<float> iter_y(map_point_cloud2_, "y");
  sensor_msgs::PointCloud2Iterator<float> iter_z(map_point_cloud2_, "z");
  sensor_msgs::PointCloud2Iterator<double> iter_th(map_point_cloud2_, "th");
  //  UINT32 is also another limit of pointcloud
  sensor_msgs::PointCloud2Iterator<uint32_t> iter_cluster_id(map_point_cloud2_, "cluster_id");
  sensor_msgs::PointCloud2Iterator<uint32_t> iter_signature_id(map_point_cloud2_, "signature_id");

  // Get iterators for first new element
  iter_x += map_point_cloud2_.width - vec.value.size();
  iter_y += map_point_cloud2_.width - vec.value.size();
  iter_z += map_point_cloud2_.width - vec.value.size();
  iter_th += map_point_cloud2_.width - vec.value.size();
  iter_cluster_id += map_point_cloud2_.width - vec.value.size();
  iter_signature_id += map_point_cloud2_.width - vec.value.size();

  bool id_overflow_detected = false;
  bool position_overflow_detected = false;
  for (unsigned int ii = 0; ii < vec.value.size();
       ii++, ++iter_x, ++iter_y, ++iter_z, ++iter_th, ++iter_cluster_id, ++iter_signature_id) {
    const float signature_x = vec.value[ii].pose.x;
    const float signature_y = vec.value[ii].pose.y;
    const float signature_z = 0.0f;
    const double signature_th = vec.value[ii].pose.th;
    if (!detail::IdIsWithinLimits(vec.value[ii].cluster_id, vec.value[ii].signature_id)) {
      id_overflow_detected = true;
    }
    if (!detail::PositionIsWithinLimits(vec.value[ii].pose.x, vec.value[ii].pose.y)) {
       position_overflow_detected = true;
    }
    uint32_t cluster_id = static_cast<uint32_t>(vec.value[ii].cluster_id);
    uint32_t signature_id = static_cast<uint32_t>(vec.value[ii].signature_id);

    *iter_x = signature_x;
    *iter_y = signature_y;
    *iter_z = signature_z;
    *iter_th = signature_th;
    *iter_cluster_id = cluster_id;
    *iter_signature_id = signature_id;
  }
  if (id_overflow_detected) {
    ROS_WARN("One or more signatures received that exceeded Cluster- or Signature Id limits of RVIZ visualization");
  }
  if (position_overflow_detected) {
    ROS_WARN("One or more signatures received that exceeded position (float) limits of RVIZ visualization");
  }
  pub_map_as_point_cloud2_.publish(map_point_cloud2_);
}

void AccerionDriverROS::CreatedSignatureAck(accerion::api::CreatedSignature created_signature) {
    accerion_driver_msgs::CreatedSignature message;
    message.timestamp = LatencyCompensatedTimestamp();
    message.signature_id = created_signature.signature.signature_id;
    message.cluster_id = created_signature.signature.cluster_id;
    message.pose.position.x = created_signature.signature.pose.x;
    message.pose.position.y = created_signature.signature.pose.y;
    message.pose.orientation = tf::createQuaternionMsgFromYaw(
        created_signature.signature.pose.th * accerion::api::kDegToRad);

    pub_created_signatures_.publish(message);
    VisualizeSignatures(accerion::api::SignatureVector{{created_signature.signature}});
}

bool AccerionDriverROS::CallbackServiceSetDateTime(accerion_driver_msgs::DateTime::Request &req,
                                                   accerion_driver_msgs::DateTime::Response &res) {
  std::string fullDate = req.date;
  std::string fullTime = req.time;

  std::vector<std::string> dateStrings, timeStrings;
  std::istringstream fullDateStream(fullDate), fullTimeStream(fullTime);
  std::string s;
  while (getline(fullDateStream, s, '/')) {
    dateStrings.push_back(s);
  }
  while (getline(fullTimeStream, s, ':')) {
    timeStrings.push_back(s);
  }
  if (dateStrings.size() != 3 || timeStrings.size() != 3) {
    res.success = false;
    res.message = "One of the fields was invalid, make sure to use the following format dd/mm/yyyy hh:mm:ss";
    return true;
  }

  accerion::api::DateTime dt;
  dt.day = static_cast<uint8_t>(std::stoi(dateStrings[0]));
  dt.month = static_cast<uint8_t>(std::stoi(dateStrings[1]));
  dt.year = static_cast<uint16_t>(std::stoi(dateStrings[2]));

  dt.hours = static_cast<uint8_t>(std::stoi(timeStrings[0]));
  dt.minutes = static_cast<uint8_t>(std::stoi(timeStrings[1]));
  dt.seconds = static_cast<uint8_t>(std::stoi(timeStrings[2]));
  auto result = accerion_sensor_->SetDateTime(dt).WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occurred. Datetime might have been set, but no acknowledgement was received..";
    return true;
  }
  if (result.value().accepted) {
    res.success = true;
    res.message = "Datetime set";
  } else {
    res.success = false;
    res.message = result.value().message;
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceGetAreas(accerion_driver_msgs::GetAreas::Request &req,
                                                accerion_driver_msgs::GetAreas::Response &res) {
  auto result = accerion_sensor_->GetAreaIds().WaitFor();
  if (result) {
    res.activeArea = result.value().active_area;
    res.availbleAreas = result.value().all_areas;
    res.success = true;
    res.message = "";
  } else {
    res.success = false;
    res.message = "Request failed, either a timeout occured or the areas could not be requested";
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceSetArea(accerion_driver_msgs::SetArea::Request &req,
                                               accerion_driver_msgs::SetArea::Response &res) {
  auto result = accerion_sensor_->SetArea(req.desiredArea).WaitFor();
  if (!result) {
    res.message = "A timeout occured";
    res.success = false;
    return true;
  }
  if (result.value().accepted) {
    res.message = "Succes";
    res.success = true;
    return true;
  } else {
    res.message = result.value().message;
    res.success = false;
    return true;
  }
}

bool AccerionDriverROS::CallbackServiceGetRamRomStats(
    accerion_driver_msgs::RamRomStatsSRV::Request &req,
    accerion_driver_msgs::RamRomStatsSRV::Response &res) {
  auto result =  accerion_sensor_->GetRamRomStats().WaitFor();
  if (!result) {
    res.success = false;
    res.message = "A timeout occurred";
    return true;
  }
  res.success = true;
  auto stats = result.value();
  res.stats.romAvailable = stats.rom_available;
  res.stats.romTotal = stats.rom_total;
  res.stats.sdAvailable = stats.sd_available;
  res.stats.sdTotal = stats.sd_total;
  res.stats.ramUsed = stats.ram_used;
  res.stats.ramTotal = stats.ram_total;
  for (auto part : stats.parts) {
    accerion_driver_msgs::DirSizePart dirPart;
    dirPart.type = static_cast<uint16_t>(part.type);
    dirPart.sizeInMB = part.size_in_mb;
    dirPart.name = part.name;
    res.stats.parts.push_back(dirPart);
  }
  return true;
}

bool AccerionDriverROS::CallbackServiceLogEvent(accerion_driver_msgs::Event::Request &req,
                                    accerion_driver_msgs::Event::Response &res) {
 accerion_sensor_->LogEvent(req.message);
 return true;
}
