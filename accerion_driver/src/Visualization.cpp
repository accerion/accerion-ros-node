/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "Visualization.h"

Visualization::Visualization()
{
    pnh_ = new ros::NodeHandle("~");
    nodeNs_ = ros::this_node::getNamespace();
    ROS_INFO("nodeNs_: %s", nodeNs_.c_str());

    pubOutputVelocity_       = nh_.advertise <geometry_msgs::TwistStamped> ("velocity", 1);
    pubOutputLinVelocity_    = nh_.advertise <std_msgs::Float32> ("lin_velocity", 1);
    pubOutputAngVelocity_    = nh_.advertise <std_msgs::Float32> ("ang_velocity", 1);
    pubConsoleOutput_        = nh_.advertise <visualization_msgs::MarkerArray>("driver_output", 100);
    pubSearchRanges_         = nh_.advertise <geometry_msgs::PolygonStamped>("search_ranges", 10);

    subsUncorrectedOdometry_ = nh_.subscribe( "odom", 1, &Visualization::callbackUncorrectedOdometry, this);
    subsCorrectedPose_       = nh_.subscribe( "corrected_odom", 1, &Visualization::callbackCorrectedPose, this);
    subsRosoutAgg_           = nh_.subscribe( "/rosout_agg", 100, &Visualization::callbackRosoutAgg, this);

    nLinesToKeep_   = 3;
    consoleOutputLines_.resize(nLinesToKeep_);
}

void Visualization::callbackUncorrectedOdometry(const nav_msgs::Odometry::ConstPtr& odomMsg)
{
    outputVelocityMsg_.header.stamp    = odomMsg->header.stamp;
    outputVelocityMsg_.header.frame_id = odomMsg->child_frame_id;
    outputVelocityMsg_.twist           = odomMsg->twist.twist;

    pubOutputVelocity_.publish(outputVelocityMsg_);

    std_msgs::Float32 linVelMsg;     
    linVelMsg.data = std::hypot(odomMsg->twist.twist.linear.x, odomMsg->twist.twist.linear.y);
    pubOutputLinVelocity_.publish(linVelMsg);

    std_msgs::Float32 angVelMsg;
    angVelMsg.data = odomMsg->twist.twist.angular.z;
    pubOutputAngVelocity_.publish(angVelMsg);
}

void Visualization::callbackCorrectedPose(const nav_msgs::Odometry::ConstPtr& poseMsg)
{
    outputVelocityMsg_.header.frame_id = poseMsg->child_frame_id;

    double searchRadiusXY     = 3.0*std::max(std::sqrt(poseMsg->pose.covariance[0]), std::sqrt(poseMsg->pose.covariance[7]));
    double searchRangeHeading = 3.0*std::sqrt(poseMsg->pose.covariance[35]);

    geometry_msgs::PolygonStamped searchRangesPolygon;

    searchRangesPolygon.header.frame_id = poseMsg->child_frame_id;
    searchRangesPolygon.header.stamp    = poseMsg->header.stamp;

    searchRangesPolygon.polygon.points.clear();

    geometry_msgs::Point32 drawPoint;

    drawPoint.x = searchRadiusXY;
    drawPoint.y = 0.0;
    drawPoint.z = 0.0;
    searchRangesPolygon.polygon.points.push_back(drawPoint);

    int pointCount = 100;
    float angOffset = 2.0*M_PI/pointCount;
    for(unsigned int ii = 0; ii < pointCount; ii++)
    {   
        utils::rotate2d(drawPoint.x, drawPoint.y, angOffset);
        searchRangesPolygon.polygon.points.push_back(drawPoint);
    }

    // searchRangesPolygon.polygon.points.push_back(drawPoint);

    pubSearchRanges_.publish(searchRangesPolygon);

}

void Visualization::callbackRosoutAgg(const rosgraph_msgs::Log::ConstPtr& outputLog)
{
    if( "/" + outputLog->name == nodeNs_ + "/accerion_driver")
    {
        consoleOutputLines_.erase(consoleOutputLines_.begin());
        consoleOutputLines_.insert(consoleOutputLines_.end(), outputLog->msg);

        visualization_msgs::MarkerArray markerArray;

        ros::Time tstamp = ros::Time::now();
        float zOffsetText = 0.0;

        for(unsigned int ii = 0; ii < consoleOutputLines_.size(); ii++)
        {
            visualization_msgs::Marker marker;
            marker.header.frame_id = "map";
            marker.header.stamp = tstamp;
            marker.ns = nodeNs_;
            marker.id = ii;
            marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
            marker.action = visualization_msgs::Marker::ADD;
            marker.scale.z = 0.025;
            marker.color.a = 0.3 + 0.7*(float)ii/(consoleOutputLines_.size()-1.0);
            marker.color.r = 0.0;
            marker.color.g = 1.0;
            marker.color.b = 1.0;
            marker.text = consoleOutputLines_[ii];
            marker.pose.position.x = 0.0;
            marker.pose.position.y = 0.0;

            float currZoffset = ( std::count(consoleOutputLines_[ii].begin(), consoleOutputLines_[ii].end(), '\n') + 1 ) * 0.035;
            zOffsetText += 0.5*currZoffset;
            marker.pose.position.z = zOffsetText;
            zOffsetText += 0.5*currZoffset;

            markerArray.markers.push_back(marker);
        }

        pubConsoleOutput_.publish(markerArray);
    }

}