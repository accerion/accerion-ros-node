#include "AccerionSearchLoopClosuresServer.h"

AccerionSearchLoopClosuresServer::AccerionSearchLoopClosuresServer(std::string name,
                                      std::shared_ptr<accerion::api::Sensor> sensor) :
                      as_(nh_, name, boost::bind(&AccerionSearchLoopClosuresServer::ExecuteCallback, this, _1), false),
                      action_name_(name) {
    sensor_ = sensor;
    as_.start();
}

void AccerionSearchLoopClosuresServer::StopSearch() {
    accerion_driver_msgs::SearchLoopClosuresResult result;
    auto future = sensor_->StopSearchForLoopClosures().WaitFor();
    if (!future) {
        result.success = false;
        result.message = "Did not receive an response from sensor";
        as_.setAborted(result);
        return;
    }
    auto value = future.value();
    result.success = value.accepted;
    result.message = value.message;
    result.success ? as_.setSucceeded(result) : as_.setAborted(result);
}

void AccerionSearchLoopClosuresServer::StartSearch(const std::string& path, double search_radius) {
    accerion_driver_msgs::SearchLoopClosuresResult result;
    std::vector<accerion::api::LoopClosure> edges;
    sensor_->SubscribeToLoopClosures().Callback([&edges](accerion::api::LoopClosure lc) {
                                                            edges.push_back(lc);
                                                        });
    auto& future_complete = sensor_->SubscribeToLoopClosuresCompleted();
    auto future = sensor_->StartSearchForLoopClosures(search_radius).WaitFor();
    if (future) {  // if not, sensor might still have started..
        auto value = future.value();
        //  Request denied
        if (!value.accepted) {
            result.success = value.accepted;
            result.message = value.message;
            as_.setAborted(result);
            return;
        }
    }
    future_complete.WaitFor(std::chrono::hours(24));
    result.success = accerion::api::LoopClosure::WriteLoopClosuresToFile(edges, path);
    if (result.success) {
        result.message = "Export of .g2o file was successful.";
        as_.setSucceeded(result);
    } else {
        result.message = "Could not save .g2o file.";
        as_.setAborted(result);
    }
}

void AccerionSearchLoopClosuresServer::ExecuteCallback(const
                                                       accerion_driver_msgs::SearchLoopClosuresGoalConstPtr &goal) {
    // Turning off
    if (!goal->enabled) {
        StopSearch();
        return;
    }

    // Starting
    StartSearch(goal->path, goal->search_radius);
}
