#include "AccerionGetLogsServer.h"
#include "AccerionSensorAPI/connection_manager.h"

AccerionGetLogsServer::AccerionGetLogsServer(std::string name, uint32_t sensor_serial) :
    as_(nh_, name, boost::bind(&AccerionGetLogsServer::ExecuteCallback, this, _1), false),
    action_name_(name) {
  sensor_serial_ = sensor_serial;
  as_.start();
}

void AccerionGetLogsServer::ExecuteCallback(const accerion_driver_msgs::GetLogsGoalConstPtr &goal) {

  auto& future_update_service = accerion::api::ConnectionManager::GetUpdateService(
                                                                  accerion::api::SerialNumber{sensor_serial_});
  std::unique_ptr<accerion::api::UpdateService> update_service = future_update_service.WaitFor(std::chrono::seconds(5));

  accerion_driver_msgs::GetLogsFeedback feedback;
  accerion_driver_msgs::GetLogsResult result;

  if (update_service == nullptr) {
    ROS_WARN("Could not connect to UpdateService. It might already be in use.");
    result.message = "Could not connect to UpdateService. It might already be in use.";
    result.done = false;
    as_.setAborted(result);
    return;
  }

  std::mutex m;
  std::condition_variable cv;

  auto progress_lambda = [&feedback, this](int progress) {
    ROS_INFO("Retrieving Logs progress: %i", progress);
    feedback.progress = progress;
    as_.publishFeedback(feedback);
  };

  auto processing_lambda = [&feedback, this](accerion::api::FileTransferTask task, int progress){
      ROS_INFO("Packing Logs progress: %i", progress);
      feedback.progress = progress;
      as_.publishFeedback(feedback);
  };

  auto done_lambda = [&result, &m, &cv](bool done, std::string msg) {
    std::unique_lock<std::mutex> lk(m);

    ROS_INFO("%s", msg.c_str());

    result.done = true;
    result.success = done;
    result.message = msg;

    lk.unlock();
    cv.notify_all();
  };


  update_service->GetLogs(goal->path, std::move(progress_lambda), std::move(processing_lambda), std::move(done_lambda));

  std::unique_lock<std::mutex> lk(m);
  cv.wait(lk, [&result] { return result.done; });

  result.success ? as_.setSucceeded(result) : as_.setAborted(result);
}