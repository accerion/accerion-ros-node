#include "AccerionRecordingsServer.h"

AccerionRecordingsServer::AccerionRecordingsServer(std::string name, std::shared_ptr<accerion::api::Sensor> sensor) :
    as_(nh_, name, boost::bind(&AccerionRecordingsServer::ExecuteCallback, this, _1), false),
    action_name_(name) {
  sensor_ = sensor;
  as_.start();
}

void AccerionRecordingsServer::ExecuteCallback(const accerion_driver_msgs::RecordingsGoalConstPtr &goal) {

  accerion_driver_msgs::RecordingsFeedback feedback;
  accerion_driver_msgs::RecordingsResult result;

  std::mutex m;
  std::condition_variable cv;

  if (goal->mode.compare("download") == 0) {
    auto transfer_progress_cb = [&feedback](int progress) {
      ROS_INFO("Recording retrieval progress: %i", progress);
      feedback.progress = progress;
    };

    auto processing_progress_cb = [this, &feedback](accerion::api::FileTransferTask task, int progress){
      ROS_INFO("Recording gathering progress: %i", progress);
      feedback.progress = progress;
    };

    auto doneLambda = [&result, &m, &cv](bool done, std::string message) {
      ROS_INFO("%s", message.c_str());

      result.done = true;
      result.success = done;
      result.message = message;
      std::unique_lock<std::mutex> lk(m);
      lk.unlock();
      cv.notify_all();
    };

    sensor_->GetRecordings(goal->recordings, goal->path, std::move(transfer_progress_cb),
                            std::move(processing_progress_cb), std::move(doneLambda));

    std::unique_lock<std::mutex> lk(m);
    cv.wait(lk, [&result] { return result.done; });
    result.success = true;
  } else if (goal->mode.compare("delete") == 0) {
    auto delete_result = sensor_->DeleteRecordings(goal->recordings).WaitFor();
    if (delete_result) {
      if (delete_result.value().value.size() > 0) {
        result.success = false;
        std::stringstream failedIndexesString;
        failedIndexesString << "The following recordings could not be deleted: ";
        for (const auto& fi : delete_result.value().value) {
          failedIndexesString << fi << ", ";
        }
        result.message = failedIndexesString.str();
      } else {
        result.success = true;
        result.message = "Recordings deleted!";
      }
    }
  } else {
    result.success = false;
    result.message = "Available options are download and delete";
  }
  result.done = true;
  result.success ? as_.setSucceeded(result) : as_.setAborted(result);
}