#include "AccerionSendMapServer.h"

AccerionSendMapServer::AccerionSendMapServer(std::string name, std::shared_ptr<accerion::api::Sensor> sensor) :
    as_(nh_, name, boost::bind(&AccerionSendMapServer::ExecuteCallback, this, _1), false),
    action_name_(name) {
  sensor_ = sensor;
  as_.start();
}

void AccerionSendMapServer::ExecuteCallback(const
                                            accerion_driver_msgs::SendMapGoalConstPtr &goal) {

  accerion_driver_msgs::SendMapFeedback feedback;
  accerion_driver_msgs::SendMapResult result;

  auto progresslambda = [&feedback, this](int progress) {
    ROS_INFO("Map transfer progress: %i", progress);
    feedback.progress = progress;
    as_.publishFeedback(feedback);
  };

  std::mutex m;
  std::condition_variable cv;

  auto done_lambda = [&result, &m, &cv](bool res, std::string msg) {
    ROS_INFO("%s", msg.c_str());

    result.done = true;
    result.success = res;
    result.message = msg;
    std::unique_lock<std::mutex> lk(m);
    lk.unlock();
    cv.notify_all();
  };

  auto processing_progress_lambda = [&feedback, this](accerion::api::FileTransferTask task, int progress){
            ROS_INFO("Processing the map...");
            feedback.progress = progress;
            as_.publishFeedback(feedback);
        };

  if (goal->placement_mode != accerion::api::GetEnumValue(accerion::api::FileTransferTask::kReplaceMap) &&
      goal->placement_mode != accerion::api::GetEnumValue(accerion::api::FileTransferTask::kMergeMap) &&
      goal->placement_mode != accerion::api::GetEnumValue(accerion::api::FileTransferTask::kUpdateMap)) {
    auto msg = "Incorrect replacement strategy";
    ROS_WARN(msg);
    result.message = msg;
    as_.setAborted(result);
    return;
  }

  accerion::api::FileTransferTask task = static_cast<accerion::api::FileTransferTask>(goal->placement_mode);
  sensor_->SendMap(goal->path, task, std::move(progresslambda), std::move(processing_progress_lambda),
                    std::move(done_lambda));

  std::unique_lock<std::mutex> lk(m);
  cv.wait(lk, [&result] { return result.done; });

  result.success ? as_.setSucceeded(result) : as_.setAborted(result);
}
