#!/bin/bash

files=$(find . -type f -name "*.urdf.xacro");
for f in $files;
do echo $f;
check_urdf <(rosrun xacro xacro $f);
done