## Tasks

- [ ] The ticket number is present in the MR title
- [ ] The changelog has been updated
- [ ] I have self-reviewed this MR according to the [coding guidelines](https://accerion.atlassian.net/wiki/spaces/IKB/pages/635043921/Coding+guidelines)
- [ ] For the code that this change impacts, I believe that the automated tests validate the functionality
- [ ] The API commit has been updated

[comment]: # (The ticket is a draft by default until we have checked all the tasks first)
/draft

## Summary

[comment]: # (Summarize the issue that this MR solves concisely)
[comment]: # (What was the previous behavior/What is the new behavior)

## Steps to test 
[comment]: # (How one can test the feature/fix)

## Relevant logs and/or screenshots
[comment]: # (Paste any relevant logs if they are not already in Jira - use code blocks ``` to format)

## Links
[comment]: # (Links to any related confluence page or merge requests)
