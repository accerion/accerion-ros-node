/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Trigger.h>
#include <tf/transform_listener.h>
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include <math.h>

#include "AccerionPoseManager.h"
#include "test_ros_common.hpp"

#include <gtest/gtest.h>

class PoseManagerTest : public ::testing::Test {
public:
    PoseManagerTest() {
        LoadParams();
        service_client_ = nh_.serviceClient<std_srvs::Trigger>(pose_manager_node_name_+"/reset");

        robot_pose_sub_ = nh_.subscribe(robot_pose_topic_, 10, &PoseManagerTest::CallbackRobotPose, this);
        ext_ref_sub_ = nh_.subscribe(ext_ref_topic_, 10, &PoseManagerTest::CallbackExtRef, this);
        
        odom_pub_ = nh_.advertise <nav_msgs::Odometry> (odom_topic_, 1);
        corrections_pub_ = nh_.advertise <nav_msgs::Odometry> (corrections_topic_, 1);

        common_ros_test::WaitUntilAlive(pose_manager_node_name_);

        std::cout << "pose_manager_tester node initialized" << std::endl;
        ros::Duration(1.0).sleep();
    }

    void LoadParams() {
        assert(nh_.hasParam("pose_manager_node_name") && "Parameter pose_manager_node_name not set");
        nh_.getParam("/pose_manager_node_name", pose_manager_node_name_);

        nh_.getParam(pose_manager_node_name_+"/publish_global_tf", publish_global_tf_);
        nh_.getParam(pose_manager_node_name_+"/publish_local_tf", publish_local_tf_);

        nh_.getParam(pose_manager_node_name_+"/corrections_topic", corrections_topic_);
        nh_.getParam(pose_manager_node_name_+"/robot_pose_topic", robot_pose_topic_);
        nh_.getParam(pose_manager_node_name_+"/ext_ref_topic", ext_ref_topic_);
        nh_.getParam(pose_manager_node_name_+"/odom_topic", odom_topic_);

        nh_.getParam(pose_manager_node_name_+"/global_parent_frame", global_parent_frame_);
        nh_.getParam(pose_manager_node_name_+"/global_child_link", global_child_link_);
        nh_.getParam(pose_manager_node_name_+"/local_parent_frame", local_parent_frame_);
        nh_.getParam(pose_manager_node_name_+"/local_child_link", local_child_link_);
        nh_.getParam(pose_manager_node_name_+"/base_link", base_link_);
        nh_.getParam(pose_manager_node_name_+"/sensor_link", sensor_link_);
    }

    void StartPublishing() {
        corrections_timer_ = nh_.createTimer(ros::Duration(1.0/7.0),
                       std::bind(&PoseManagerTest::PubCorrections, this));
        odometry_timer_ = nh_.createTimer(ros::Duration(1.0/25.0),
                       std::bind(&PoseManagerTest::PubOdom, this));
        ros::Duration(1.0).sleep();
    }

    void PubCorrections() const {
        nav_msgs::Odometry msg;

        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = global_parent_frame_;
        msg.child_frame_id = global_child_link_;
        msg.pose.pose.position.x = 2.;
        msg.pose.pose.orientation.w = 1.;

        corrections_pub_.publish(msg);
    }
    
    void PubOdom() const {
        nav_msgs::Odometry msg;

        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = local_parent_frame_;
        msg.child_frame_id = local_child_link_;
        msg.pose.pose.position.y = -2.;
        msg.pose.pose.orientation.w = 1.;

        odom_pub_.publish(msg);
    }

    tf::StampedTransform PoseToStampedTf(const geometry_msgs::Pose& pose, ros::Time timestamp, std::string frame_id, const std::string& child_frame_id) {
        tf::Transform tf;
        tf::poseMsgToTF(pose, tf);
        return tf::StampedTransform(tf, timestamp, frame_id, child_frame_id);
    }

protected:
    void CallbackRobotPose(const geometry_msgs::PoseWithCovarianceStamped& msg) {
        robot_pose_msg_ = msg;
        robot_pose_msg_received_ = true;
    }

    void CallbackExtRef(const geometry_msgs::PoseStamped& msg) {
        ext_ref_msg_ = msg;
        ext_ref_msg_received_ = true;
    }

    ros::NodeHandle nh_;

    bool ext_ref_msg_received_{false}, robot_pose_msg_received_{false}; 
    geometry_msgs::PoseWithCovarianceStamped robot_pose_msg_;
    geometry_msgs::PoseStamped ext_ref_msg_;

    std::string global_parent_frame_, global_child_link_; 
    std::string local_parent_frame_, local_child_link_, base_link_, sensor_link_;

    ros::ServiceClient service_client_;

    ros::Publisher odom_pub_, corrections_pub_;
    ros::Subscriber robot_pose_sub_, ext_ref_sub_, tf_sub_;

    std::string pose_manager_node_name_;
    std::string corrections_topic_, robot_pose_topic_, ext_ref_topic_, odom_topic_;
    bool publish_global_tf_, publish_local_tf_;

    tf::TransformListener tf_listener_;

    ros::Timer corrections_timer_, odometry_timer_;
};