/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include "pose_manager_ros_test.hpp"
#include <std_srvs/Trigger.h>

#include <gtest/gtest.h>

// For testing the transforms, the following tree is used: 
// Trees:          |------- map ------|- robot -|
// Tree id's:    global -> local -> common -> local_child
// (x,y,th):       (1,1,-90) (1,-2,90) (0,1,-90)
// Tree id's:                       common -> global_child
// (x,y,th):                            (3,0,0)

TEST_F(PoseManagerTest, CheckSubscribing) {      
    EXPECT_TRUE(common_ros_test::CheckSubscribing({odom_pub_, corrections_pub_}));
}

TEST_F(PoseManagerTest, CheckAdvertising) {
    EXPECT_TRUE(common_ros_test::CheckAdvertising({robot_pose_sub_, ext_ref_sub_}));
}

TEST_F(PoseManagerTest, CheckExistanceOfTfFrames) {
    // the local and global parent frames are sent upon initialization when publish_global_tf is true
    EXPECT_TRUE(publish_global_tf_);
    EXPECT_TRUE(tf_listener_.frameExists(local_parent_frame_));
    EXPECT_TRUE(tf_listener_.frameExists(global_parent_frame_));
}

TEST_F(PoseManagerTest, TestCorrectMessagePublishingPoseManagerOdomCb) {
    PubOdom();
    ros::Duration(1.0).sleep();

    EXPECT_TRUE(ext_ref_msg_received_);
    EXPECT_TRUE(robot_pose_msg_received_);
}

// this test needs to be ran before publishing any transforms
TEST_F(PoseManagerTest, TestInitializationOfTf) {
    const auto global_to_local = common_ros_test::LookupTransform(tf_listener_, global_parent_frame_, local_parent_frame_);
    common_ros_test::CheckStampedTransform(global_to_local, {0.,0.,0.});
}

TEST_F(PoseManagerTest, TestCorrectPublishedPoseManagerMessages) {
    StartPublishing();

    EXPECT_TRUE(ext_ref_msg_received_ && robot_pose_msg_received_)
        << "external reference and/or robot pose messages were not received.";
       
    // external reference is global_parent_frame -> sensor_link
    common_ros_test::CheckStampedTransform(PoseToStampedTf(ext_ref_msg_.pose, ext_ref_msg_.header.stamp,
                                                            ext_ref_msg_.header.frame_id, sensor_link_), {-1.,1.,-90.});
    // roboot pose is global_parent_frame -> base_link
    common_ros_test::CheckStampedTransform(PoseToStampedTf(robot_pose_msg_.pose.pose, robot_pose_msg_.header.stamp,
                                                            robot_pose_msg_.header.frame_id, base_link_), {-1.,0.,0.});
}

TEST_F(PoseManagerTest, TestCorrectUpdatingTfTree) {
    StartPublishing();

    EXPECT_TRUE(publish_global_tf_ && publish_local_tf_)
        << "Parameters publish_global_tf and publish_local_tf should be true to check the values of the published transforms";
    
    const auto local_to_base = common_ros_test::LookupTransform(tf_listener_, local_parent_frame_, base_link_);  
    common_ros_test::CheckStampedTransform(local_to_base, {1.,-2.,90.});

    const auto global_to_local = common_ros_test::LookupTransform(tf_listener_, global_parent_frame_, local_parent_frame_);
    common_ros_test::CheckStampedTransform(global_to_local, {1.,1.,-90.});
}

TEST_F(PoseManagerTest, TestResetService) {
    StartPublishing();

    std_srvs::Trigger srv;

    ASSERT_TRUE(service_client_.call(srv)) << "Failed to call service reset";

    const auto local_to_child = common_ros_test::LookupTransform(tf_listener_, local_parent_frame_, local_child_link_);
    const auto local_to_child_invers = tf::StampedTransform(tf::Transform(local_to_child.inverse()),
                                                          ros::Time::now(), local_child_link_, local_parent_frame_);
    common_ros_test::CheckStampedTransform(local_to_child_invers, {0.,2.,0.});
}

// Run all the tests that were declared with TEST_F()
int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "pose_manager_tester");
    ros::AsyncSpinner spinner(1);
    spinner.start();
    int ret = RUN_ALL_TESTS();
    spinner.stop();
    return ret;
}