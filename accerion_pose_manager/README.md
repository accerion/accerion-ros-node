# 1. Requirements

Make sure you followed the general setup instruction of the Accerion ROS driver.

- To verify that your setup is complete, do:
```bash
rospack find accerion_driver
```
The response should be:
```bash
/home/your-user-name/codes/catkin_ws/src/accerion-ros-node/accerion_driver
```

# 2. Sample launch

If you have an Accerion sensor running in the same network as your ROS master, do:

`roslaunch accerion_driver triton_pose_manager_mapping.launch`

to bring up the Accerion ROS Driver, RViz visualization and the Accerion pose manager. There are 3 example launch files for Accerion pose manager depending on whether you are localizing or mapping:
* `triton_pose_manager_mapping.launch`
* `triton_pose_manager_mapping_amcl.launch`
* `triton_pose_manager_mapping_localization.launch`

If you have multiple Accerion sensors in your local network, you need to specify the serial number of the sensor by using the `serial_number` argument as:

`roslaunch accerion_driver triton_pose_manager_mapping.launch serial_number:=598100101`

Note: If you do not specify the `serial_number`, it will be assigned `0` by default. As a result, the driver will pair with the first Accerion sensor it detects in the network.

# 3. Overview

## 3.1. Generic TF tree

Highlighting the different TF chains.

| Global (parent)- to global-child frame | Local (parent)- to local-child frame| External reference input
| ------------- | ------------- | ------------- |
| ![Generic TF tree global](doc/TIG_ROS_tf-Generic-Global.png) | ![Generic TF tree local](doc/TIG_ROS_tf-Generic-Local.png) | ![Generic TF tree](doc/TIG_ROS_tf-Generic-extRef.png)


## 3.2. Specific TF tree

For robot with wheel encoders, Lidar- and Triton sensor.

|![Specific  TF tree](doc/TIG_ROS_tf-Mapping.png)|
| ------------- | 

## 3.2.1 Mapping mode
Highlighting TF chains during mapping mode.

| Global TF chain | Local TF chain | External reference TF chain
| ------------- | ------------- | ------------- |
| ![Generic TF tree](doc/TIG_ROS_tf-Mapping-Lidar.png)  | ![Generic TF tree global](doc/TIG_ROS_tf-Mapping-Odometry.png) | ![Generic TF tree local](doc/TIG_ROS_tf-Mapping-Triton.png)
| <ul><li>Set **publish_global_tf** parameter to **True**</li> <li>Receive map → lidar message with timestamp **t<sub>correction</sub>**</li> <li>Update map → odomfor TF tree at **t<sub>correction</sub>**</li></ul>| <ul><li>Receive odom → wheel odometry with **t<sub>odom</sub>**</li> <li>Update odom → robot at **t<sub>odom</sub>** <br>**Note: if **publish_local_tf** set to **True***</li> <li>Triggers external reference TF chain</li></ul> |<ul><li>Lookup map → Triton from TF tree for **t<sub>latest</sub>**</li> <li>Prepare ext. ref. message and set timestamp to **t<sub>latest</sub>**</li> <li>Publish message to Accerion driver</li></ul>  



## 3.2.2 Localization mode
Highlighting TF chains during localization mode. *Note Triton takes over the role of
global child frame, replacing the Lidar.

| Global TF chain | Local TF chain | External reference TF chain
| ------------- | ------------- | ------------- |
| ![Generic TF tree](doc/TIG_ROS_tf-Localization-TritonDriftCorrections.png)  | ![Generic TF tree global](doc/TIG_ROS_tf-Localization-WheelOdometry.png) | ![Generic TF tree local](doc/TIG_ROS_tf-Localization-Triton.png)
| <ul><li>Set **publish_global_tf parameter** to **True**</li> <li>Triton recognizes floor and ouputs drift correction: <br> map → Triton with timestamp **t<sub>correction</sub>**</li> <li>Similar to a Lidar pose you update: <br> map → odom for TF tree at **t<sub>correction</sub>**</li></ul>| <ul><li>Receive odom → wheel odometry with **t<sub>odom</sub>**</li> <li>Update odom → robot at **t<sub>odom</sub>** <br>**Note: if **publish_local_tf** set to **True***</li> <li>Triggers external reference TF chain</li></ul> |<ul><li>Lookup map → Triton from TF tree for **t<sub>latest</sub>**</li> <li>Prepare ext. ref. message and set timestamp to **t<sub>latest</sub>**</li> <li>Publish message to Accerion driver</li></ul>

## 3.2.3 Additional SLAM package
Use of additional SLAM packages (Google Cartographer, AMCL, Gmapping, etc).

|![Specific  TF tree](doc/TIG_ROS_tf-Mapping-SLAM.png)|
| ------------- | 
| <ul><li>Set **publish_global_tf** parameter to **False**</li> <li>While mapping, map→odom taken care of by SLAM package</li></ul>

# 4. Nodes

## 4.1. accerion_pose_manager

![Accerion pose manager overview](doc/TIG_ROS_tf-AccerionPoseManager.png)

### 4.1.1. Subscribed Topics 
**odom_topic (nav_msgs/Odometry, default "odom")**
- Topic where the pose manager can find odometry data

**corrections_topic (nav_msgs/Odometry, default "corrections")**
- Topic where the pose manager can find drift corrections (Triton or certain Lidar based systems)

### 4.1.2. Published Topics
**robot_pose_topic (geometry_msgs/PoseWithCovarianceStamped, default "robot_pose")**
- Corrected pose of the robot base frame

**ext_ref_topic (geometry_msgs/PoseStamped, default "ext_ref")**
- Topic used by Triton to efficiently search for matches in its database

### 4.1.3. Parameters
**publish_global_tf (bool, default "false")**
- Update global_parent_frame -> local_parent_frame for each correction received at the **corrections_topic** topic

**publish_local_tf (bool, default "false")**
- Update local_parent_frame -> base_link (odom -> base_link)
- Publish robot pose to the robot_pose_topic

### 4.1.4. Frames/Links
**global_parent_frame**
- Usually map

**global_child_link**
- Global source of positioning with discrete (irregular) updates ; e.g. Lidar/Visual SLAM link, Triton link, used to update global_frame -> local_frame.

**local_parent_frame**
- Usually odom

**local_child_link**
- Local source of positioning with continuous updates in the local_frame; e.g.  wheel odometry (base_link or base_footprint), 
  flow sensor (sensor link), visual odometry (camera link), Triton relative (triton link).

**base_link**
- Base link of the robot

**sensor_link**
- Triton link on the robot
