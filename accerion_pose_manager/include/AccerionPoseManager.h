/* Copyright (c) 2021, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef _AccerionPoseManager_H_
#define _AccerionPoseManager_H_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include <tf/transform_broadcaster.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Trigger.h>

class AccerionPoseManager {
 public:
  AccerionPoseManager(ros::NodeHandle &nh);

  void initialize();
  void loadParameters();

 private:
  //
  void initPoseCb(const geometry_msgs::PoseWithCovarianceStamped::Ptr &initPoseMsg);

  // compute reference- and robot pose from incoming local odometry
  void odomCb(const nav_msgs::Odometry::Ptr &odomMsg);
  // process drift corrections to compensate local odometry
  void correctionsCb(const boost::shared_ptr<const nav_msgs::Odometry> &odomMsg);

  // ros service clients/servers
  ros::ServiceServer resetPoseManagerService_;
  bool resetPoseManager(std_srvs::Trigger::Request &req,
                        std_srvs::Trigger::Response &res);

  ros::NodeHandle pnh_;
  std::string nodeNamespace_;

  // ros topic names
  std::string initPoseTopic_;
  std::string odomTopic_;
  std::string correctionsTopic_;
  std::string robotPoseTopic_;
  std::string extRefTopic_;

  // parameters
  bool processCorrections_;
  bool processOdometry_;

  // subscribers and publishers
  ros::Subscriber initPoseSub_;
  ros::Subscriber odomSub_;
  message_filters::Subscriber<nav_msgs::Odometry> correctionsSub_;
  tf::MessageFilter<nav_msgs::Odometry> *tfFilter_;
  ros::Publisher robotPosePub_;
  ros::Publisher extRefPub_;

  // ros tf names
  std::string globalParentFrame_;
  std::string globalChildLink_;
  std::string localParentFrame_;
  std::string localChildLink_;
  std::string baseLink_;
  std::string sensorLink_;

  tf::TransformListener tfListener_;
  tf::TransformBroadcaster tfBroadcaster_;
  tf2_ros::StaticTransformBroadcaster tfStaticBroadcaster_;
};

#endif