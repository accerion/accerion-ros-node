/* Copyright (c) 2021, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <AccerionPoseManager.h>

AccerionPoseManager::AccerionPoseManager(ros::NodeHandle &nh) {
  pnh_ = nh;
  nodeNamespace_ = ros::this_node::getNamespace();
}

void AccerionPoseManager::initialize() {
  robotPosePub_ = pnh_.advertise<geometry_msgs::PoseWithCovarianceStamped>(robotPoseTopic_, 1);
  extRefPub_ = pnh_.advertise<geometry_msgs::PoseStamped>(extRefTopic_, 1);

  odomSub_ =
      pnh_.subscribe(odomTopic_, 1, &AccerionPoseManager::odomCb, this, ros::TransportHints().tcpNoDelay());

  initPoseSub_ =
      pnh_.subscribe(initPoseTopic_, 1, &AccerionPoseManager::initPoseCb, this, ros::TransportHints().tcpNoDelay());

  if (processCorrections_) {
    correctionsSub_.subscribe(pnh_, correctionsTopic_, 1, ros::TransportHints().tcpNoDelay());
    tfFilter_ = new tf::MessageFilter<nav_msgs::Odometry>(correctionsSub_, tfListener_, globalChildLink_, 1);
    tfFilter_->registerCallback(boost::bind(&AccerionPoseManager::correctionsCb, this, _1));

    geometry_msgs::TransformStamped globalToLocalTf;
    globalToLocalTf.header.stamp = ros::Time::now();
    globalToLocalTf.header.frame_id = globalParentFrame_;
    globalToLocalTf.child_frame_id = localParentFrame_;
    tf::transformTFToMsg(tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0, 0, 0)), globalToLocalTf.transform);
    tfStaticBroadcaster_.sendTransform(globalToLocalTf);
  }

  resetPoseManagerService_ = pnh_.advertiseService("reset", &AccerionPoseManager::resetPoseManager, this);
}

void AccerionPoseManager::loadParameters() {
  processCorrections_ = pnh_.param("publish_global_tf", false);
  processOdometry_ = pnh_.param("publish_local_tf", false);

  initPoseTopic_ = ros::names::resolve(pnh_.param<std::string>("init_pose_topic", "/initialpose"));
  odomTopic_ = ros::names::resolve(pnh_.param<std::string>("odom_topic", "odom"));
  correctionsTopic_ = ros::names::resolve(pnh_.param<std::string>("corrections_topic", "corrections"));
  robotPoseTopic_ = ros::names::resolve(pnh_.param<std::string>("robot_pose_topic", "robot_pose"));
  extRefTopic_ = ros::names::resolve(pnh_.param<std::string>("ext_ref_topic", "ext_ref"));

  globalParentFrame_ = tfListener_.resolve(pnh_.param<std::string>("global_parent_frame", "map"));
  globalChildLink_ = tfListener_.resolve(pnh_.param<std::string>("global_child_link", "lidar_link"));
  localParentFrame_ = tfListener_.resolve(pnh_.param<std::string>("local_parent_frame", "odom"));
  localChildLink_ = tfListener_.resolve(pnh_.param<std::string>("local_child_link", "base_link"));
  baseLink_ = tfListener_.resolve(pnh_.param<std::string>("base_link", "base_link"));
  sensorLink_ = tfListener_.resolve(pnh_.param<std::string>("sensor_link", "sensor_link"));

}

void AccerionPoseManager::initPoseCb(const geometry_msgs::PoseWithCovarianceStamped::Ptr &initPoseMsg) {
  // Construct global->base from initPoseMsg
  tf::Transform globalToBase;
  globalToBase.setOrigin(tf::Vector3(
      initPoseMsg->pose.pose.position.x,
      initPoseMsg->pose.pose.position.y,
      0.0));

  globalToBase.setRotation(tf::Quaternion(
      initPoseMsg->pose.pose.orientation.x,
      initPoseMsg->pose.pose.orientation.y,
      initPoseMsg->pose.pose.orientation.z,
      initPoseMsg->pose.pose.orientation.w));

  // Compose global->base with base->local for drift global->local
  tf::StampedTransform baseToLocal;
  try {
    tfListener_.lookupTransform(baseLink_, localParentFrame_, ros::Time(0), baseToLocal);

  } catch (...) {
    ROS_INFO("RobotPose: lookupTransform error! Does the tf from %s to %s exist?",
             baseLink_.c_str(), localParentFrame_.c_str());
  }

  // Compute drift global->local
  tf::Transform globalRefToLocal = globalToBase * baseToLocal;

  geometry_msgs::TransformStamped globalToLocalTf;
  globalToLocalTf.header.stamp = initPoseMsg->header.stamp;
  globalToLocalTf.header.frame_id = globalParentFrame_;
  globalToLocalTf.child_frame_id = localParentFrame_;
  tf::transformTFToMsg(globalRefToLocal, globalToLocalTf.transform);
  tfStaticBroadcaster_.sendTransform(globalToLocalTf);
}

void AccerionPoseManager::odomCb(const nav_msgs::Odometry::Ptr &odomMsg) {
  // Construct local->localChild from odomMsg
  tf::Transform localToChild;

  localToChild.setOrigin(tf::Vector3(
      odomMsg->pose.pose.position.x,
      odomMsg->pose.pose.position.y,
      0.0));

  localToChild.setRotation(tf::Quaternion(
      odomMsg->pose.pose.orientation.x,
      odomMsg->pose.pose.orientation.y,
      odomMsg->pose.pose.orientation.z,
      odomMsg->pose.pose.orientation.w));

  // 1) Compose local->localChild with localChild->base results in local->base
  tf::StampedTransform localChildToBase;
  try {
    tfListener_.lookupTransform(localChildLink_, baseLink_, ros::Time(0), localChildToBase);

  } catch (...) {
    ROS_INFO("RobotPose: lookupTransform error! Does the tf from %s to %s exist?",
             localChildLink_.c_str(), baseLink_.c_str());
  }

  tf::Transform localToBase;
  localToBase = localToChild * localChildToBase;

  if (processOdometry_) {
    // Broadcast local->base
    tfBroadcaster_.sendTransform(tf::StampedTransform(localToBase,
                                                      odomMsg->header.stamp, localParentFrame_, baseLink_));
  }

  //2) Compose global->local with local->base for global->base
  tf::StampedTransform globalToLocal;
  try {
    tfListener_.lookupTransform(globalParentFrame_, localParentFrame_, ros::Time(0), globalToLocal);

  } catch (...) {
    ROS_INFO("RobotPose: lookupTransform error! Does the tf from %s to %s exist?",
             globalParentFrame_.c_str(), localParentFrame_.c_str());
  }

  tf::Transform globalToBase = globalToLocal * localToBase;

  // Publish robot pose
  tf::Vector3 robotPose = globalToBase.getOrigin();
  tf::Quaternion robotOrientation = globalToBase.getRotation();

  geometry_msgs::PoseWithCovarianceStamped poseMsg;

  poseMsg.header.stamp = odomMsg->header.stamp;
  poseMsg.header.seq = odomMsg->header.seq;
  poseMsg.header.frame_id = globalParentFrame_;
  poseMsg.pose.pose.position.x = robotPose.x();
  poseMsg.pose.pose.position.y = robotPose.y();
  poseMsg.pose.pose.position.z = robotPose.z();
  poseMsg.pose.pose.orientation.x = robotOrientation.x();
  poseMsg.pose.pose.orientation.y = robotOrientation.y();
  poseMsg.pose.pose.orientation.z = robotOrientation.z();
  poseMsg.pose.pose.orientation.w = robotOrientation.w();

  robotPosePub_.publish(poseMsg);

  //3) Compose global->base with base->sensor and publish as ext_ref
  tf::StampedTransform baseToSensor;
  try {
    tfListener_.lookupTransform(baseLink_, sensorLink_, ros::Time(0), baseToSensor);
  }

  catch (...) {
    ROS_INFO("RobotPose: lookupTransform error! Does the tf from %s to %s exist?",
             baseLink_.c_str(), sensorLink_.c_str());
  }
  tf::Transform globalToSensor = globalToBase * baseToSensor;

  tf::Vector3 sensorPosition = globalToSensor.getOrigin();
  tf::Quaternion sensorOrientation = globalToSensor.getRotation();

  geometry_msgs::PoseStamped extRefMsg;

  extRefMsg.header.stamp = odomMsg->header.stamp;
  extRefMsg.header.seq = odomMsg->header.seq;
  extRefMsg.header.frame_id = globalParentFrame_;
  extRefMsg.pose.position.x = sensorPosition.x();
  extRefMsg.pose.position.y = sensorPosition.y();
  extRefMsg.pose.orientation.x = sensorOrientation.x();
  extRefMsg.pose.orientation.y = sensorOrientation.y();
  extRefMsg.pose.orientation.z = sensorOrientation.z();
  extRefMsg.pose.orientation.w = sensorOrientation.w();

  extRefPub_.publish(extRefMsg);
}

void AccerionPoseManager::correctionsCb(const boost::shared_ptr<const nav_msgs::Odometry> &odomMsg) {
  // Construct global->globalChild from odomMsg
  tf::Transform globalToChild;
  globalToChild.setOrigin(tf::Vector3(
      odomMsg->pose.pose.position.x,
      odomMsg->pose.pose.position.y,
      0.0));

  globalToChild.setRotation(tf::Quaternion(
      odomMsg->pose.pose.orientation.x,
      odomMsg->pose.pose.orientation.y,
      odomMsg->pose.pose.orientation.z,
      odomMsg->pose.pose.orientation.w));

  // Compose global->globalChild with globalChild->local for drift global->local
  tf::StampedTransform globalChildToLocal;
  try {
    tfListener_.lookupTransform(globalChildLink_, localParentFrame_, odomMsg->header.stamp, globalChildToLocal);
  } catch (...) {
    ROS_INFO("RobotPose: lookupTransform error! Does the tf from %s to %s exist?",
             globalChildLink_.c_str(), localParentFrame_.c_str());
  }

  // Compute drift global->local
  tf::Transform globalRefToLocal = globalToChild * globalChildToLocal;

  geometry_msgs::TransformStamped globalToLocalTf;
  globalToLocalTf.header.stamp = odomMsg->header.stamp;
  globalToLocalTf.header.frame_id = globalParentFrame_;
  globalToLocalTf.child_frame_id = localParentFrame_;
  tf::transformTFToMsg(globalRefToLocal, globalToLocalTf.transform);
  tfStaticBroadcaster_.sendTransform(globalToLocalTf);
}

bool AccerionPoseManager::resetPoseManager(std_srvs::Trigger::Request &req,
                                           std_srvs::Trigger::Response &res) {
  // Reset robot pose by setting global->local as the inverse of local->localChild
  tf::StampedTransform localToChild;

  try {
    tfListener_.lookupTransform(localParentFrame_, localChildLink_, ros::Time(0), localToChild);

  } catch (...) {
    ROS_INFO("RobotPose: lookupTransform error! Does the tf from %s to %s exist?",
             localParentFrame_.c_str(), localChildLink_.c_str());
  }

  tfBroadcaster_.sendTransform(tf::StampedTransform(tf::Transform(localToChild.inverse()),
                                                    ros::Time::now(),
                                                    globalParentFrame_,
                                                    localParentFrame_));

  res.success = true;
  res.message = "Reset pose successful";

  return true;
}

